# Evaluate staticdeps

## Against valgrind-depsim dynamic deps

Evaluate against CesASMe's benchmarks --- depends also on `gdb_bb_occur`'s part
of CesASMe to find basic blocks.

1. Use `valgrind-depsim`'s `deps_all.sh` script on a collection of binary
   benchmarks --- here, CesASMe generated benchmarks.
   Save the results as `$DS_RES_ROOT/$(basename "$bench").deps`
    * Re-run multiple times if some processes OOM'd, reducing parallelism
        * Results are cached anyway
2. In a virtualenv containing at least
    * CesASMe
    * staticdeps
    * the patched capstone version
   run in a Python REPL
   ```python
   kerns = vg_depsim.find_kernels(CESASME_BENCHS_ROOT)
   res = vg_depsim.analyze_kernels(kerns, DS_RES_ROOT, parallel=...)
   # Inspect either/both
   res.success_rate()
   res.success_rate_weighted()
   ```

If evaluating multiple lifetimes, modify the lifetime setting in depsim's
`ds_instrument.c`, recompile and rerun `deps_all.sh` (**change the output
directory** to avoid overwriting results), re-run evaluation --- the staticdeps
results are cached, it will be faster this time.

## Time evaluation

After the previous evaluation is carried, timing data of all the runs is
available. `vg_depsim.py` also contains tools to aggregate and evaluate this
data.

Producing the timings data sequence and saving it:
```python3
import vg_depsim
from pathlib import Path
kerns = vg_depsim.find_kernels(CESASME_BENCHS_ROOT)
times = vg_depsim.AnalyzeTimes(kerns, Path(DS_RES_ROOT))
times.to_json('./time_eval.json.gz')  # this file is gitted
```

Loading the data sequence:
```python3
import vg_depsim
times = vg_depsim.AnalyzeTimes.from_json('./time_eval.json.gz')
```

Producing analyses:
```python3
...
from matplotlib import pyplot as plt
# Plotting box plots
times.boxplot()
plt.savefig('./time_boxplot.svg')
times.speedup_boxplot() ;
plt.savefig('./speedup_boxplot.svg')

# Data sequences, examples
import numpy as np
np.median(times.depsim_seq())
np.median(times.staticdeps_seq())
np.median(times.staticdeps_sum_seq())
np.median(times.speedup_seq())
```
