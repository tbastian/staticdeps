#include <stdlib.h>

static const char* ERR_STR = "";

const char *perf_pipedream_strerror(int errcode) { return ERR_STR; }
int perf_pipedream_query_event(int event_code) { return 0; }
int perf_pipedream_library_init(int version) { return version; }
int perf_pipedream_is_initialized() { return 1; }
int perf_pipedream_event_name_to_code(const char *event_name, int *event_code) {
    *event_code = 1;
    return 0;
}
int perf_pipedream_create_eventset(int *event_set) { return 0; }
int perf_pipedream_add_event(int event_set, int event_idx) { return 0; }
int perf_pipedream_remove_event(int event_set, int event_idx) { return 0; }
int perf_pipedream_start(int event_set) { return 0; }
int perf_pipedream_read(int event_set, long long res[]) { return 0; }
int perf_pipedream_stop(int event_set, long long res[]) { exit(0); }
int perf_pipedream_cleanup_eventset(int event_set) { return 0; }
int perf_pipedream_destroy_eventset(int *event_set) { return 0; }
void perf_pipedream_shutdown() {}
