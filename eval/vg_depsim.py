import hashlib
import multiprocessing
import pickle
import typing as t
import time
import json
import gzip
from collections import defaultdict
from csv import DictReader
from dataclasses import dataclass, field
from matplotlib import pyplot
from pathlib import Path

import tqdm
from genbenchs import prog
from staticdeps import capstone_deps, elf_reader
from tbastian_pyutils import basic_blocks


@dataclass
class DepsimDep:
    src: int
    dst: int
    count: int


@dataclass
class DepRes:
    binary: Path
    src: int
    dst: int
    vg_count: int


@dataclass
class AnalysisRes:
    found: list[DepRes] = field(default_factory=list)
    missed: list[DepRes] = field(default_factory=list)

    def __iadd__(self, oth: "AnalysisRes") -> "AnalysisRes":
        self.found += oth.found
        self.missed += oth.missed
        return self

    @property
    def count(self) -> int:
        """Total number of dependencies (missed or found)"""
        return len(self.found) + len(self.missed)

    def success_rate(self) -> float:
        return len(self.found) / self.count

    def success_rate_weighted(self) -> float:
        weight_found = sum(map(lambda x: x.vg_count, self.found))
        weight_missed = sum(map(lambda x: x.vg_count, self.missed))
        return weight_found / (weight_found + weight_missed)


CACHE_DIR = Path(__file__).parent / "_cache"


def timed_call(func):
    def wrapped(*args, **kwargs):
        perf_start = time.perf_counter_ns()
        proc_start = time.process_time_ns()
        out = func(*args, **kwargs)
        perf_elapsed = time.perf_counter_ns() - perf_start
        proc_elapsed = time.process_time_ns() - proc_start

        return (out, perf_elapsed, proc_elapsed)

    return wrapped


def kernel_cachedir(kernel: Path) -> Path:
    with kernel.open("rb") as handle:
        hasher = hashlib.sha1(handle.read(), usedforsecurity=False)
        binary_checksum = hasher.hexdigest()
    return CACHE_DIR / (kernel.name + "." + binary_checksum)


class KernelAnalysis:
    class NoGdbAnalysis(Exception):
        """Raised when no GDB analysis of this kernel was found"""

    class AsmNoMatch(Exception):
        """Raised when no GDB analysis of this kernel was found"""

    path: Path
    symbol: str
    depsim_report: Path

    cache_base: Path
    bbs: list[prog.BasicBlock]
    bb_decoder: basic_blocks.BBDecoder
    bb_addr: list[basic_blocks.Block]
    depsim_deps_at: dict[int, list[DepsimDep]]

    _depsim_periodic_deps: t.Optional[
        dict[basic_blocks.Block, list[capstone_deps.PeriodicDep]]
    ]

    MIN_INSN: int = 1024

    def __init__(self, kernel: Path, symbol: str, depsim_report: Path):
        self.path = kernel
        self.symbol = symbol
        self.depsim_report = depsim_report

        self.cache_base = kernel_cachedir(self.path)
        self.cache_base.mkdir(parents=True, exist_ok=True)

        self._depsim_periodic_deps = None

        recovered_bbs = prog.GdbAnalysis(self.path, self.symbol, False).restore()
        if recovered_bbs is None:
            raise self.NoGdbAnalysis(self.path.as_posix())
        self.bbs = recovered_bbs

        self.bb_decoder = basic_blocks.BBDecoder(self.path)
        elf_sym = self.bb_decoder.get_symbol(self.symbol)

        offset = elf_sym.addr
        self.bb_addr = []
        for blk_id, blk in enumerate(self.bbs):
            size = len(blk.with_control.raw_bytes)
            blk_range = basic_blocks.Block(addr=offset, size=size)
            real_bytes = self.bb_decoder.elf_text_bytes(blk_range)
            if real_bytes != blk.with_control.raw_bytes:
                if blk_id + 1 == len(self.bbs):
                    # Last block -- sometimes compilers add a weird block
                    blk_range = basic_blocks.Block(addr=elf_sym.end - size, size=size)
                    real_bytes = self.bb_decoder.elf_text_bytes(blk_range)
                    if real_bytes == blk.with_control.raw_bytes:
                        self.bb_addr.append(blk_range)
                        continue
                raise self.AsmNoMatch(
                    f"Expected {blk.with_control.raw_bytes.hex()} at "
                    f"{self.path.as_posix()}:0x{offset:x}-0x{offset+size:x}"
                )
            self.bb_addr.append(blk_range)
            offset += size

        self.depsim_deps_at = defaultdict(list)
        self._load_depsim_deps()

    def _load_depsim_deps(self):
        deps = []
        with self.depsim_report.open("r") as h:
            reader = DictReader(
                h, fieldnames=["count", "from_addr", "from_file", "to_addr", "to_file"]
            )
            for row in reader:
                if (
                    Path(row["from_file"]).name == self.path.name
                    and row["from_file"] == row["to_file"]
                ):
                    deps.append(
                        DepsimDep(
                            src=int(row["from_addr"], 16),
                            dst=int(row["to_addr"], 16),
                            count=int(row["count"]),
                        )
                    )
        for dep in deps:
            self.depsim_deps_at[dep.src].append(dep)

    def make_depsim_periodic_deps(
        self,
    ) -> dict[basic_blocks.Block, list[capstone_deps.PeriodicDep]]:
        """Make, for each block, a list of periodic deps from depsim data (deps +
        hits). The at_iter parameter is always set to 1, as we do not have this
        information. Dependencies are present if their count is higher than 80% of the
        block's hits."""

        out: dict[basic_blocks.Block, list[capstone_deps.PeriodicDep]] = {}
        for b_id, block in enumerate(self.bb_addr):
            deps_list = []

            block_data = self.bb_decoder.get_block_data(block, disasm=True)
            addr_to_insn_id = {
                insn.address: i_id for i_id, insn in enumerate(block_data.disasm)
            }

            # There are not that many bbs, we can just iterate multiple times
            for src, dsts in self.depsim_deps_at.items():
                if src not in block:
                    continue
                for dep in dsts:
                    if dep.dst not in block:
                        continue

                    if dep.count >= 0.8 * self.bbs[b_id].hits:
                        # Periodic dep.
                        deps_list.append(
                            capstone_deps.PeriodicDep(
                                src=addr_to_insn_id[dep.src],
                                dst=addr_to_insn_id[dep.dst],
                                at_iter=-1,
                            )
                        )

            out[block] = deps_list
        return out

    @property
    def depsim_periodic_deps(
        self,
    ) -> dict[basic_blocks.Block, list[capstone_deps.PeriodicDep]]:
        if self._depsim_periodic_deps is None:
            self._depsim_periodic_deps = self.make_depsim_periodic_deps()
        return self._depsim_periodic_deps

    def write_depsim_periodic_deps(self) -> None:
        """Write the periodic deps extracted from dynamic Valgrind Depsim analysis to
        pickled files, one per analyzed block, in the same directory as
        `self.depsim_report`."""
        per_deps = self.depsim_periodic_deps
        for block_id, block in enumerate(self.bb_addr):
            deps = per_deps[block]
            deps_path = self.depsim_report.with_suffix(
                f".dyn_periodic_deps.{block_id}.pickle"
            )
            with deps_path.open("wb") as handle:
                pickle.dump(deps, handle)

    def relevant_blocks(self) -> t.Iterator[int]:
        """Lists the blocks that occur a significant number of times"""
        max_occur = max((bb.hits for bb in self.bbs))
        threshold = max_occur // 10
        for bb_id, block in enumerate(self.bbs):
            if block.hits >= threshold:
                yield bb_id

    def _staticdeps_for_block_nocache(
        self, b_id: int
    ) -> list[tuple[capstone_deps.PeriodicDep, tuple[int, int]]]:
        bb = self.bbs[b_id]
        addr_range = self.bb_addr[b_id]

        elf = elf_reader.ElfReader(
            self.path.as_posix(),
            with_bytes=bb.with_control.raw_bytes,
            offset=addr_range.addr,
        )
        cs_deps = capstone_deps.CapstoneDependencies.from_elf_reader(
            elf, self.__class__.MIN_INSN, only_mem_deps=True
        )
        kern = cs_deps.unrepeated_kernel

        straight_deps = cs_deps.get_dependencies()
        per_deps = list(capstone_deps.analyze_deps(straight_deps, cs_deps).keys())

        dep_tuples: list[tuple[int, int]] = []
        for dep in per_deps:
            dep_tuples.append(
                (
                    kern[dep.src].address,
                    kern[dep.dst].address,
                )
            )

        return list(zip(per_deps, dep_tuples))

    def staticdeps_for_block(
        self, b_id: int
    ) -> list[tuple[capstone_deps.PeriodicDep, tuple[int, int]]]:
        cache_path = self.cache_base / f"{b_id:03d}.pickle"
        time_path = self.cache_base / f"{b_id:03d}.time"
        if cache_path.exists():
            # Restore cached data if available
            with cache_path.open("rb") as h:
                cached_data = pickle.load(h)
                if cached_data is not None and isinstance(cached_data, list):
                    return cached_data

        out, perf_elapsed, proc_elapsed = timed_call(
            self._staticdeps_for_block_nocache
        )(b_id)

        with time_path.open("w") as h:
            h.write(f"Total (ns): {perf_elapsed}\nProcess (ns): {proc_elapsed}")

        # cache dependencies data
        with cache_path.open("wb") as h:
            pickle.dump(out, h)

        return out

    def analyze_block(self, b_id: int, with_raw_deps: bool = False) -> AnalysisRes:
        addr_range = self.bb_addr[b_id]
        static_deps = self.staticdeps_for_block(b_id)

        res = AnalysisRes()

        if with_raw_deps:
            static_deps_found = set(dep_tuple for (_, dep_tuple) in static_deps)
            for dep_src, dep_list in self.depsim_deps_at.items():
                if dep_src in addr_range:
                    for vg_dep in dep_list:
                        if vg_dep.dst in addr_range:
                            depres = DepRes(
                                binary=self.path,
                                src=vg_dep.src,
                                dst=vg_dep.dst,
                                vg_count=vg_dep.count,
                            )
                            if (vg_dep.src, vg_dep.dst) in static_deps_found:
                                res.found.append(depres)
                            else:
                                res.missed.append(depres)
        else:
            dynamic_deps = self.depsim_periodic_deps[addr_range]
            static_deps_set = set((dep.src, dep.dst) for (dep, _) in static_deps)
            for dep in dynamic_deps:
                depres = DepRes(binary=self.path, src=dep.src, dst=dep.dst, vg_count=1)
                if (dep.src, dep.dst) in static_deps_set:
                    res.found.append(depres)
                else:
                    res.missed.append(depres)

        return res

    def analyze_relevant_blocks(self, with_raw_deps: bool = False) -> AnalysisRes:
        res = AnalysisRes()
        for blk_id in self.relevant_blocks():
            res += self.analyze_block(blk_id, with_raw_deps=with_raw_deps)
        return res


def analyze_one(
    kernel: Path, depsim: Path, with_raw_deps: bool
) -> t.Optional[AnalysisRes]:
    try:
        kern_analysis = KernelAnalysis(kernel, "kernel", depsim)
        return kern_analysis.analyze_relevant_blocks(with_raw_deps=with_raw_deps)
    except KernelAnalysis.AsmNoMatch:
        print(f"Badly matched kernel: {kernel.as_posix()}")
        return None


def analyze_one_star(arg: tuple[Path, Path, bool]) -> t.Optional[AnalysisRes]:
    return analyze_one(*arg)


def analyze_kernels(
    kernels: list[Path],
    depsim_root: Path,
    parallel: t.Optional[int] = None,
    with_raw_deps: bool = False,
) -> AnalysisRes:
    """Analyze the coverage of staticdeps dependencies.
    :depsim_root: is the directory in which depsim's results are stored.
    :parallel: controls the number of parallel processes (defaults to the main process
        only)
    :with_raw_deps: if true, do not transform dyndeps into staticdeps-like dependencies
        before assessing the coverage rate. In particular, do not discard spurious
        dependencies.
    """
    res = AnalysisRes()
    if not parallel:
        for kern_path in tqdm.tqdm(kernels):
            vg_path = depsim_root / (kern_path.name + ".deps")
            if not vg_path.exists():
                continue
            c_res = analyze_one(kern_path, vg_path, with_raw_deps)
            if c_res is not None:
                res += c_res
    else:
        arg_list: list[tuple[Path, Path, bool]] = []
        for kern_path in kernels:
            vg_path = depsim_root / (kern_path.name + ".deps")
            if not vg_path.exists():
                continue
            arg_list.append((kern_path, vg_path, with_raw_deps))

        with multiprocessing.Pool(parallel) as pool:
            for val in tqdm.tqdm(
                pool.imap_unordered(analyze_one_star, arg_list, chunksize=10),
                total=len(arg_list),
            ):
                if val is not None:
                    res += val

    return res


def generate_depsim_perdeps(kernels: list[Path], depsim_root: Path) -> None:
    """Calls `write_depsim_periodic_deps` on all kernels"""
    for kern_path in kernels:
        vg_path = depsim_root / (kern_path.name + ".deps")
        if not vg_path.exists():
            continue
        try:
            kern_analysis = KernelAnalysis(kern_path, "kernel", vg_path)
            kern_analysis.write_depsim_periodic_deps()
        except KernelAnalysis.AsmNoMatch:
            print(f"Badly matched kernel: {kern_path.as_posix()}")


class TimeResult(t.NamedTuple):
    """Run time of a task in milliseconds -- this is the best resolution achieveable
    by /bin/time anyway"""

    real: int  # total elapsed time
    process: int  # process time only

    def serialize(self):
        return {
            "real": self.real,
            "process": self.process,
        }

    @classmethod
    def unserialize(cls, vals):
        return cls(**vals)


def read_unixtimefile(path: Path) -> TimeResult:
    """Reads a unix `time`d command output at path"""
    real = None
    process = None

    def read_one(line: str) -> int:
        return round(float(line.strip().split(" ", maxsplit=1)[1]) * 1000)

    with path.open("r") as handle:
        for line in handle:
            if line.startswith("real"):
                real = read_one(line)
            elif line.startswith("user"):
                process = read_one(line)
    return TimeResult(real=real, process=process)


def read_analyzetimefile(path: Path) -> TimeResult:
    """Reads a time file produced by `analyze_kernels`"""
    real = None
    process = None

    def read_one(line: str) -> int:
        return round(float(line.strip().split(": ")[1]) / 1e6)

    with path.open("r") as handle:
        for line in handle:
            if line.startswith("Total"):
                real = read_one(line)
            elif line.startswith("Process"):
                process = read_one(line)
    return TimeResult(real=real, process=process)


class AnalyzeTimes:
    """Analyze process times of vg_depsim and staticdeps"""

    class KernTimes(t.NamedTuple):
        depsim: TimeResult
        staticdeps: list[TimeResult]

        def tot_staticdeps(self) -> TimeResult:
            return TimeResult(
                real=sum(block.real for block in self.staticdeps),
                process=sum(block.process for block in self.staticdeps),
            )

        def serialize(self):
            return {
                "depsim": self.depsim.serialize(),
                "staticdeps": [x.serialize() for x in self.staticdeps],
            }

        @classmethod
        def unserialize(cls, vals):
            return cls(
                depsim=TimeResult.unserialize(vals["depsim"]),
                staticdeps=[TimeResult.unserialize(x) for x in vals["staticdeps"]],
            )

    kernels: list[Path]
    timed_vg_root: Path
    kernel_times: dict[Path, KernTimes]

    def __init__(self, kernels: list[Path], timed_vg_root: Path, kernel_times=None):
        self.kernels = kernels
        self.timed_vg_root = timed_vg_root
        if kernel_times is None:
            self.gather_times()
        else:
            self.kernel_times = kernel_times

    def serialize_times(self):
        return {k.as_posix(): v.serialize() for k, v in self.kernel_times.items()}

    def to_json(self, out_path):
        with gzip.open(out_path, "wb") as h:
            h.write(
                json.dumps(
                    {
                        "kernels": [x.as_posix() for x in self.kernels],
                        "timed_vg_root": self.timed_vg_root.as_posix(),
                        "kernel_times": self.serialize_times(),
                    }
                ).encode("utf-8")
            )

    @classmethod
    def from_json(cls, in_path):
        with gzip.open(in_path, "rb") as h:
            raw_dict = json.loads(h.read().decode("utf-8"))
        kernels = [Path(x) for x in raw_dict["kernels"]]
        timed_vg_root = Path(raw_dict["timed_vg_root"])
        kernel_times = {
            Path(k): cls.KernTimes.unserialize(v)
            for k, v in raw_dict["kernel_times"].items()
        }
        return cls(kernels, timed_vg_root, kernel_times)

    def depsim_seq(self) -> list[int]:
        return [kern.depsim.real for kern in self.kernel_times.values()]

    def staticdeps_sum_seq(self) -> list[int]:
        return [
            sum(time.real for time in kern.staticdeps)
            for kern in self.kernel_times.values()
        ]

    def staticdeps_seq(self) -> list[int]:
        return [
            time.real for kern in self.kernel_times.values() for time in kern.staticdeps
        ]

    def speedup_seq(self) -> list[float]:
        """Series of (forall bb, depsim_time(bb) / staticdeps(bb))"""
        out = []
        for kern in self.kernel_times.values():
            for bb in kern.staticdeps:
                out.append(kern.depsim.real / bb.real)
        return out

    def boxplot(self):
        pyplot.clf()
        seqs = [
            self.depsim_seq(),
            self.staticdeps_sum_seq(),
            self.staticdeps_seq(),
        ]
        labels = [
            "Depsim",
            "Staticdeps (blocks sum)",
            "Staticdeps (one block)",
        ]
        pyplot.xticks(rotation=8)
        pyplot.ylabel("Dependencies computation time (ms)")
        pyplot.yscale("log")
        pyplot.boxplot(seqs, labels=labels)

    def speedup_boxplot(self):
        pyplot.clf()
        seqs = [self.speedup_seq()]
        labels = ["Staticdeps speedup"]
        pyplot.ylabel("Speedup (depsim/staticdeps time)")
        pyplot.boxplot(seqs, labels=labels)

    def gather_times(self):
        self.kernel_times = {}

        for kern in self.kernels:
            try:
                depsim_time = read_unixtimefile(
                    self.timed_vg_root / (kern.name + ".deps.time")
                )
            except FileNotFoundError:
                continue

            staticdeps_times = []
            cachedir = kernel_cachedir(kern)

            analysis_times_paths: list[Path] = []
            if not cachedir.is_dir():
                continue

            for res in cachedir.iterdir():
                if res.name.endswith(".time"):
                    analysis_times_paths.append(res)
            if not analysis_times_paths:
                continue
            analysis_times_paths.sort()
            for path in analysis_times_paths:
                staticdeps_times.append(read_analyzetimefile(path))

            self.kernel_times[kern] = self.__class__.KernTimes(
                depsim=depsim_time, staticdeps=staticdeps_times
            )


def find_kernels(root: Path) -> list[Path]:
    def dfs(base: Path, kerns: list[Path]):
        if base.is_dir():
            for sub in base.iterdir():
                dfs(sub, kerns)
        elif base.suffix == ".kernel":
            kerns.append(base)

    kerns: list[Path] = []
    dfs(root, kerns)
    return kerns
