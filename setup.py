#!/usr/bin/env python3

from setuptools import setup, find_packages
from collections import defaultdict
import typing as t
import re
import sys


def parse_requirements() -> dict:
    req_re = re.compile(r"(?P<dep>[^\[]*)(?:\[(?P<group>[^\]]+)\])?")
    reqs: dict[t.Optional[str], list[str]] = defaultdict(list)
    with open("requirements.txt", "r") as handle:
        for line in handle:
            if line.startswith("-") or line.startswith("git+"):
                continue
            if match := req_re.fullmatch(line):
                reqs[match["group"]].append(match["dep"])
    return reqs


deps = parse_requirements()
base_deps = deps.pop(None)
setup(
    name="staticdeps",
    version="0.0.3",
    description="Statically find data dependencies from a binary file",
    author="CORSE",
    license="LICENSE",
    packages=find_packages(),
    include_package_data=True,
    package_data={"staticdeps": ["py.typed"]},
    long_description=open("README.md").read(),
    install_requires=base_deps,
    extras_require=deps,
    entry_points={
        "console_scripts": [
            ("staticdeps-test = staticdeps.entrypoints:test_kernel"),
            ("staticdeps-export = staticdeps.entrypoints:export_kernel"),
        ]
    },
)
