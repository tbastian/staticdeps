from abc import ABC, abstractmethod
from pathlib import Path
import typing as t


class Dependency(t.NamedTuple):
    src_pc: int
    dst_pc: int

    def __str__(self):
        return f"0x{self.src_pc:X} → 0x{self.dst_pc:X}"

    def __eq__(self, oth):
        return self.src_pc == oth.src_pc and self.dst_pc == oth.dst_pc

    def __le__(self, oth):
        return (self.src_pc, self.dst_pc) <= (oth.src_pc, oth.dst_pc)


Dependencies = list[Dependency]


class PeriodicDep(t.NamedTuple):
    """A mean to represent a periodic dependency"""

    src: int
    dst: int
    at_iter: int

    @classmethod
    def from_dep(cls, dep: Dependency, period: int) -> "PeriodicDep":
        src_iter = dep.src_pc // period
        dst_iter = dep.dst_pc // period
        diff_iter = dst_iter - src_iter
        return cls(src=dep.src_pc % period, dst=dep.dst_pc % period, at_iter=diff_iter)
