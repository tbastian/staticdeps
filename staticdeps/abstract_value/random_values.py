""" Abstract values to use for memory and registers to track dependencies -- patch
fresh random values each time a new storage location is encountered, track arithmetic
operations, assume it will be unique. """

import copy
import logging
import operator
import random
import re
import typing as t
import warnings
from collections import defaultdict

import archinfo
import capstone
import numpy as np

from staticdeps.capstone_helper import RegCanonicalizer

logger = logging.getLogger(__name__)


class RandValException(Exception):
    """Generic exception on random values"""


class ArchNotImplemented(RandValException):
    pass


class BadCast(RandValException):
    """Thrown when trying to cast a StoredValue to a bad type, eg uint12"""


class BadRegTranslation(RandValException):
    """Thrown when a register cannot be converted from capstone to valgrind"""


class BadTypeMerge(RandValException):
    """Thrown when types cannot be reconciled around an operator"""


class BadOperatorTypes(RandValException):
    """Thrown when an operator cannot be applied to the types passed"""


T = t.TypeVar("T")

_SPECS_RE = re.compile(r"(?P<sign>u?)int(?P<size>[0-9]+)")


def type_for_size(size: int, signed: bool = False) -> type:
    if size == 1:
        return bool
    ntype_name = f"{'u' if not signed else ''}int{size}"
    try:
        return getattr(np, ntype_name)
    except AttributeError as exn:
        raise BadCast(f"No such type: {ntype_name}") from exn


def _type_selector(
    t1: t.Type[np.integer], t2: t.Type[np.integer]
) -> t.Type[np.integer]:
    """Selects the best type for the result of an operation on values of type t1 and
    t2"""

    def specs(t: t.Type[np.integer]) -> tuple[bool, int]:
        name = t.__name__
        match = _SPECS_RE.match(name)
        assert match
        return match.group("sign") != "u", int(match.group("size"))

    t1_sn, t1_sz = specs(t1)
    t2_sn, t2_sz = specs(t2)

    if t1_sn != t2_sn:
        raise BadTypeMerge(f"Types {t1} and {t2} have mismatching signedness")
    sz = max(t1_sz, t2_sz)
    return type_for_size(sz, signed=t1_sn)


def _mkoper(
    oper, signed_cast: bool = False
) -> t.Callable[[T, T | int | np.integer], T]:
    def func(e1, e2):
        if not e1.valid:
            return e1
        debug = e1.debug
        typ = type(e1.np_value)
        e2typ = e1.np_value.__class__
        if signed_cast:
            e2typ = e1.signed_np_value.__class__
        if isinstance(e2, int) or isinstance(e2, np.integer):
            if isinstance(e2, int):
                e2v = e2typ(e2)
            else:
                e2v = e2
                if not isinstance(e2v, e2typ):
                    e2v = e2typ(e2v)
                typ = _type_selector(typ, type(e2))
            e2acc = True
        else:
            if not e2.valid:
                return e2
            debug = debug or e2.debug
            if signed_cast:
                e2v = e2.signed_np_value
            else:
                e2v = e2.np_value
            e2acc = e2.accurate
            typ = _type_selector(typ, type(e2.np_value))
        try:
            with warnings.catch_warnings():  # Ignore overflows
                warnings.simplefilter("ignore", category=RuntimeWarning)
                np_e1 = e1.signed_np_value if signed_cast else e1.np_value
                nval = oper(np_e1, e2v)
                out = e1.__class__(
                    nval, accurate=e1.accurate and e2acc, typ=typ, debug=debug
                )
        except ArithmeticError:
            out = e1.__class__.invalid()
        except TypeError as exn:
            e2_str: str
            if isinstance(e2, StoredValue):
                e2_str = f"{e2} <{type(e2.np_value).__name__}>"
            else:
                e2_str = f"{e2} <{type(e2).__name__}>"

            raise BadOperatorTypes(
                "Could not compute ({} <{}>) {} ({})".format(
                    e1,
                    type(e1.np_value).__name__,
                    oper.__name__,
                    e2_str,
                )
            ) from exn
        if debug:
            logger.debug("%s %s %s -> %s", e1, oper.__name__, e2, out)
        return out

    return func


class StoredValue:
    MODULUS = 1 << 64
    _value: np.integer
    valid: bool
    accurate: bool  # Whether the value can be trusted as precise, eg. for conditions

    # Whether this value and its derivates should log debug entries upon operations.
    debug: bool

    __add__ = _mkoper(operator.add)
    __sub__ = _mkoper(operator.sub)
    __mul__ = _mkoper(operator.mul)
    __lshift__ = _mkoper(operator.lshift)
    __rshift__ = _mkoper(operator.rshift)
    __mod__ = _mkoper(operator.mod)
    __and__ = _mkoper(operator.and_)
    __or__ = _mkoper(operator.or_)
    __xor__ = _mkoper(operator.xor)
    rshift_arith = _mkoper(operator.rshift, signed_cast=True)

    def __init__(
        self,
        value: int,
        typ: t.Type[np.integer] = np.uint64,
        valid: bool = True,
        accurate: bool = True,
        debug: bool = False,
    ):
        self._value = t.cast(
            np.integer, np.array(value).astype(typ)[()]
        )  # Woah, this is circumvoluted.
        self.valid = valid
        self.accurate = accurate
        self.debug = debug

    def copy(self) -> "StoredValue":
        """Return a copy of this StoredValue"""
        return copy.deepcopy(self)

    @classmethod
    def invalid(cls):
        """Return an invalid value"""
        return cls(0, valid=False)

    @classmethod
    def fresh(cls, typ: t.Type[np.integer] = np.uint64):
        """Return a freshly random-generated value"""
        # 1<<60: leave a bit of headspace for multiplications
        return cls(random.randint(0, 1 << 60), typ=typ, accurate=False)

    @property
    def value(self) -> int:
        return int(self._value)

    @property
    def np_value(self) -> np.integer:
        return self._value

    @property
    def signed_np_value(self) -> np.integer:
        stype = type_for_size(8 * self._value.data.nbytes, signed=True)
        return stype(self._value)

    def cast(
        self, bitsize: int, fromsize: t.Optional[int] = None, signed: bool = False
    ) -> "StoredValue":
        """Cast the underlying value to a different bitsize, set signed/unsigned and
        return the new value. Does not modify in-place."""

        ntype = type_for_size(bitsize)
        nval = self._value
        if signed:
            if fromsize is None:
                fromsize = 8 * self._value.data.nbytes
            stype = type_for_size(fromsize, signed=True)
            nval = stype(nval)
        nval = ntype(nval)
        nbox = self.copy()
        nbox._value = nval
        return nbox

    def __str__(self) -> str:
        if not self.valid:
            return f"<{self.__class__.__name__}: invalid>"
        return f"<{self.__class__.__name__}: 0x{self.value:x}>"

    def __repr__(self) -> str:
        if not self.valid:
            return f"<{self.__class__.__name__}(valid=False)"
        return f"{self.__class__.__name__}(0x{self.value:x})"

    def __eq__(self, oth) -> bool:
        if isinstance(oth, int) or isinstance(oth, np.integer):
            return self.valid and self.value == oth

        if isinstance(oth, StoredValue):
            if self.valid != oth.valid:
                return False
            if self.valid:
                return self._value == oth._value
            return True
        return False

    def __hash__(self) -> int:
        return self.value if self.valid else -1


CsRegId: t.TypeAlias = int
VexRegId: t.TypeAlias = int
MemAddr: t.TypeAlias = int


class ShadowState:
    regs: dict[VexRegId, StoredValue]
    memory: dict[MemAddr, StoredValue]
    reg_canonicalizer: RegCanonicalizer

    def __init__(self, arch: archinfo.Arch):
        self.regs = defaultdict(StoredValue.fresh)
        self.memory = defaultdict(StoredValue.fresh)
        self.reg_canonicalizer = RegCanonicalizer(arch)

    def get_cs_reg(self, cs_reg: int) -> StoredValue:
        """Retrieve the current value of a register with a capstone index, or fail"""
        base_reg = self.reg_canonicalizer.cs_to_vex(cs_reg)
        if not base_reg:
            raise BadRegTranslation(f"Cannot find VEX equivalent to CS reg {cs_reg}")
        return self.regs[base_reg[0]]

    def get_memloc(self, mem) -> StoredValue:
        if isinstance(mem, capstone.x86.X86OpMem):
            base = self.get_cs_reg(mem.base) if mem.base else StoredValue(0)
            index = self.get_cs_reg(mem.index) if mem.index else StoredValue(0)
            return StoredValue(mem.disp) + index * mem.scale + base
        if isinstance(mem, capstone.arm64.Arm64OpMem):
            base = self.get_cs_reg(mem.base) if mem.base else StoredValue(0)
            index = self.get_cs_reg(mem.index) if mem.index else StoredValue(0)
            return StoredValue(mem.disp) + index + base
        raise ArchNotImplemented(f"Arch not implemented for {mem.__class__.__name__}.")


class WriteLocs:
    reg_written: dict[CsRegId, int]
    mem_written: dict[StoredValue, int]

    def __init__(self):
        self.reg_written = {}
        self.mem_written = {}
