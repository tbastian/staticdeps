""" Vaguely track pointers (displacement-index-base-scale) """

import typing as t
import logging
import capstone
from staticdeps.abstract_deps import Dependency, Dependencies
from staticdeps.capstone_helper import CsAccess, CsX86Reg

logger = logging.getLogger(__name__)


RegId: t.TypeAlias = CsX86Reg
Address: t.TypeAlias = int


class MemLocation(t.NamedTuple):
    disp: Address = 0
    base: RegId = CsX86Reg.INVALID
    index: RegId = CsX86Reg.INVALID
    scale: int = 1

    @classmethod
    def from_x86operand(cls, op: capstone.x86.X86Op) -> "MemLocation":
        return cls(
            disp=op.mem.disp,
            base=CsX86Reg(op.mem.base),
            index=CsX86Reg(op.mem.index),
            scale=op.mem.scale,
        )

    def matches(self, oth: "MemLocation") -> bool:
        """Whether two memory locations are matching, that is, are considered
        equivalent up to a few loop cycles with unknown loop variable/policy"""

        DISP_MATCH_THRESHOLD = 1024  # Threshold to consider that a displacement is in
        # fact an address or an offset
        base_match = (
            self.base == oth.base
            and self.index == oth.index
            and self.scale == oth.scale
        )
        if not base_match:
            return False
        if self.disp == oth.disp:
            return True
        return self.disp < DISP_MATCH_THRESHOLD and oth.disp < DISP_MATCH_THRESHOLD


class WriteLocations:
    """An abstract interpretation state: at some program point, a list of locations
    that last wrote registers, and a list of locations that wrote memory addresses"""

    regs: dict[RegId, list[Address]]
    mem: dict[MemLocation, list[Address]]

    def __init__(self, regs, mem):
        self.regs = regs
        self.mem = mem

    def copy(self) -> "WriteLocations":
        """Return an object holding shallow copies of the member dicts"""
        return self.__class__(
            regs=self.regs.copy(),
            mem=self.mem.copy(),
        )

    def union(self, other: "WriteLocations") -> "WriteLocations":
        """Returns a WriteLocations representing the union of the two objects"""

        out = self.copy()
        for attr in "regs", "mem":
            for k, v in getattr(other, attr).items():
                if k in getattr(out, attr):
                    getattr(out, attr)[k] += v
                else:
                    getattr(out, attr)[k] = v
        return out

    @classmethod
    def empty_locs(cls) -> "WriteLocations":
        return cls({}, {})

    @classmethod
    def multi_union(cls, *locs: "WriteLocations") -> "WriteLocations":
        if len(locs) == 0:
            return cls.empty_locs()
        cur_locs = locs[0]
        for n_loc in locs[1:]:
            cur_locs = cur_locs.union(n_loc)
        return cur_locs
