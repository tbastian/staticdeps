import typing as t
import angr
import cle
import pyvex
import capstone
import archinfo
import logging
import copy
import operator
import re
import io
import collections
from . import iaca_marks
from math import ceil
from pathlib import Path

logger = logging.getLogger(__name__)


class SymbolNotFound(Exception):
    """Raised when the requested symbol is not found"""


class MissingInfoSymbol(Exception):
    """Raised when the info symbol (kernel_with_…) is required but not found"""


class BadSymbol(Exception):
    """Raised when something unexpected happens in this symbol"""


class BadKernel(Exception):
    """Raised when the kernel found is somehow considered invalid"""


_KERNEL_PROPERTIES_RE = re.compile(
    r"kernel_with_(?P<iterations>\d+)_iterations_(?P<wordmult>\d+)_wordmults_(?P<wordadd>\d+)_wordadds"
)


class KernelProperties(t.NamedTuple):
    wordadd: int
    wordmult: int
    iterations: int


class BlkType(t.Protocol):
    def successors(self) -> t.Iterable["BlkType"]:
        ...

    @property
    def addr(self) -> int:
        ...


# An SCCList is a recursive structure abstractly defined as:
# SCCList := LoopNest of SCCList list | Block of BlkType
# Hence, each list represents a block nest (or the toplevel); each element being either
# a block or a loop nest.
# SCCList = t.Union[BlkType, t.Iterable["SCCList"]]
SCCList = t.Any  # Cyclic definitions not supported yet


class _TarjanSCC:
    blks: set[BlkType]
    index: dict[BlkType, int]
    lowlink: dict[BlkType, int]
    stack: list[BlkType]
    cur_index: int
    ignore_backedges: set[BlkType]

    def __init__(
        self, blk_set: list[BlkType], ignore_backedges: t.Optional[list[BlkType]] = None
    ):
        self.blks = set(blk_set)
        self.index = {}
        self.lowlink = {}
        self.stack = []
        self.cur_index = 0
        self.sccs: SCCList = []
        self.ignore_backedges = set(ignore_backedges or [])

    def __call__(self) -> SCCList:
        for blk in self.blks:
            if blk not in self.index:
                self._do_visit(blk)
        return self.sccs

    def _do_visit(self, block: BlkType):
        self.index[block] = self.cur_index
        self.lowlink[block] = self.cur_index
        self.cur_index += 1
        self.stack.append(block)

        if block not in self.ignore_backedges:
            for succ in block.successors():
                if succ not in self.blks:
                    continue
                if succ not in self.index:
                    self._do_visit(succ)
                    self.lowlink[block] = min(self.lowlink[block], self.lowlink[succ])
                elif succ in self.stack:
                    self.lowlink[block] = min(self.lowlink[block], self.index[succ])

        if self.lowlink[block] == self.index[block]:
            scc_list: list[BlkType] = []
            while True:
                topstack = self.stack.pop()
                scc_list.append(topstack)
                if topstack == block:
                    break
            if len(scc_list) > 1:
                scc_bottom = max(scc_list, key=lambda x: x.addr)
                scc = self.__class__(scc_list, ignore_backedges=[scc_bottom])()
            else:
                single_elt = scc_list[0]
                if single_elt in single_elt.successors():
                    scc = scc_list
                else:
                    scc = single_elt
            # Insert in front: given the graph is crawled roughly following the
            # execution order (usually, this means ordered by address increasing); this
            # will give a list in the expected order
            self.sccs.insert(0, scc)


class AngrKernelExtractor:
    proj: angr.Project
    elf: Path
    symbol: str

    _sym: cle.backends.Symbol
    _cfg: angr.analyses.cfg.cfg_fast.CFGFast
    _func: angr.knowledge_plugins.functions.function.Function

    def __init__(self, elf: Path, symbol: str):
        self.elf = elf
        self.symbol = symbol
        self.proj = angr.Project(
            elf, load_options={"auto_load_libs": False}, main_opts={"base_addr": 0x0}
        )

        _sym = self.proj.loader.find_symbol(self.symbol)
        if _sym is None:
            raise SymbolNotFound(self.symbol)
        self._sym = _sym
        self._cfg = self.proj.analyses.CFGFast(  # type: ignore
            regions=[(self._sym.rebased_addr, self._sym.rebased_addr + self._sym.size)]
        )
        self._func = self._cfg.functions.get_by_addr(self._sym.rebased_addr)
        if not self._func:
            raise BadSymbol(f"Function not found for symbol {self.symbol}")

    def export_raw_kernel(self, handle: io.BufferedIOBase, keep_jump: bool = False):
        """Exports the raw instruction bytes to the provided stream."""

        kernel = self.extract_kernel(keep_jump)
        for insn in kernel:
            handle.write(insn.bytes)

    def get_info_label(self) -> str:
        for sym in map(lambda x: x.name, self.proj.loader.symbols):
            if sym.startswith("kernel_with_"):
                return sym
        raise MissingInfoSymbol(self.elf)

    def sym_disassembly(self) -> list[capstone.CsInsn]:
        """Return the disassembly of the whole symbol"""
        return list(
            map(
                lambda x: x.insn,
                self.proj.factory.block(
                    self._sym.rebased_addr, size=self._sym.size
                ).disassembly.insns,
            )
        )

    def check_with_iaca_marks(self) -> bool:
        """Check that the extracted kernel matches the one pointed by IACA marks"""

        insns = self.sym_disassembly()

        def _dump_insn_set(iset: set[int]) -> str:
            return "\n".join(map(lambda x: str(insns[x]), iset))

        iaca_start, iaca_end = iaca_marks.find_iaca_marks_or_fail(insns)
        iaca_start += iaca_marks.IACA_MARK_INSNS

        kernel = self.extract_kernel(keep_jump=True)
        kernel_start = 0
        kernel_end = 0
        for pos, insn in enumerate(insns):
            if insn.address == kernel[0].address:
                kernel_start = pos
            if insn.address == kernel[-1].address:
                kernel_end = pos

        logger.debug(
            "IACA: %d - %d / 0x%x - 0x%x",
            iaca_start,
            iaca_end,
            insns[iaca_start].address,
            insns[iaca_end].address,
        )
        logger.debug(
            "Kern: %d - %d / 0x%x - 0x%x",
            kernel_start,
            kernel_end,
            insns[kernel_start].address,
            insns[kernel_end].address,
        )

        kernel_set = set(range(kernel_start, kernel_end + 1))
        iaca_set = set(range(iaca_start, iaca_end + 1))
        missing = iaca_set - kernel_set
        extra = kernel_set - iaca_set

        if missing:
            logger.error("Kernel misses instructions:\n%s", _dump_insn_set(missing))
        if extra:
            logger.error("Kernel has extra instructions:\n%s", _dump_insn_set(extra))
        return bool(missing or extra)

    @property
    def kernel_properties(self) -> KernelProperties:
        """The kernel properties as provided by the info label"""
        info_label = self.get_info_label()
        match = _KERNEL_PROPERTIES_RE.fullmatch(info_label)
        if not match:
            raise BadKernel(f"Bad kernel info label: {info_label}")
        props: dict[str, int] = {k: int(v) for k, v in match.groupdict().items()}
        return KernelProperties(**props)

    def extract_kernel(self, keep_jump: bool = False) -> list[capstone.CsInsn]:
        """Extracts the selected kernel from the elf data at the provided symbol. If
        :keep_jump: is False (default), removes the trailing jump instruction."""

        # Find the most nested loop: strongly connected components
        def most_nested(scc) -> t.Tuple[SCCList, int, int]:
            """Find the most nested loop, returns it with its nesting depth"""
            assert scc  # Always expect at least one block
            deepest = scc
            deepest_sz = -1
            depth = 0
            for elt in scc:
                if isinstance(elt, list):
                    c_deepest, c_depth, c_deepest_sz = most_nested(elt)
                    c_depth += 1
                    if c_depth > depth:
                        deepest = c_deepest
                        depth = c_depth
                        deepest_sz = c_deepest_sz
                    elif c_depth == depth:
                        """When multiple blocks are at equal depths, we heuristically
                        select:
                        * one that has only one basic block (seems to be always the
                          case for most-nested loop bodies);
                        * that has the highest PC;
                        * except if the previously selected has at least twice the code
                          size (if so we assume it to be a postlude)
                        """
                        if len(elt) > 1:
                            continue
                        if c_deepest_sz < 2 * deepest_sz:
                            continue
                        deepest = c_deepest
                        depth = c_depth
                        deepest_sz = c_deepest_sz
            if depth == 0:
                deepest_sz = sum(map(lambda x: x.size, scc))
            return deepest, depth, deepest_sz

        sccs = _TarjanSCC(list(self._func.nodes))()
        inner_loop, _, _ = most_nested(sccs)

        if len(inner_loop) != 1:  # The most nested loop should have only one bb
            # Check whether this is actually a single too-long basic block
            prev = inner_loop[0]
            for cur_blk in inner_loop[1:]:
                if len(prev.successors()) > 1 or len(cur_blk.predecessors()) > 1:
                    bbs = ", ".join(map(str, inner_loop))
                    raise BadKernel(
                        f"{self.elf}: "
                        + f"found most nested loop with multiple basic blocks: {bbs}"
                    )
                prev = cur_blk
        inner_insns: list[capstone.CsInsn] = []
        for blk in inner_loop:
            inner_insns += list(
                map(
                    lambda x: x.insn,
                    self._func.get_block(blk.addr).disassembly.insns,
                )
            )
        if not keep_jump:
            inner_insns = inner_insns[:-1]  # Strip the last insn: jump
        return inner_insns

    def build_unrolled_kernel(
        self, min_insn: int
    ) -> t.Tuple[list[capstone.CsInsn], int]:
        """Builds a periodic kernel from the elf data at the provided symbol.
        :returns: this unrolled kernel, alongside with its period (how long was the
        kernel before unrolling)"""

        inner_insns = self.extract_kernel(keep_jump=False)

        required_unroll = ceil(min_insn / len(inner_insns))
        out_kernel: list[capstone.CsInsn] = []
        for _ in range(required_unroll):
            out_kernel += inner_insns
        return out_kernel, len(inner_insns)
