import collections
import logging
import typing as t
from math import ceil
from pathlib import Path

import archinfo
import capstone
import pyvex

from staticdeps import vex_semantics
from staticdeps.abstract_deps import Dependencies, Dependency, PeriodicDep
from staticdeps.abstract_value import random_values as abstract_value
from staticdeps.capstone_helper import (
    CsAccess,
    RegCanonicalizer,
    get_cs_operand_type,
    capstone_hotfix_access,
    capstone_hotfix_regs_access,
)
from staticdeps.elf_reader import ElfReader

if t.TYPE_CHECKING:
    from staticdeps.angr_deps import AngrKernelExtractor
logger = logging.getLogger(__name__)


class BadOperand(Exception):
    """Raised when an invalid asm operand is encountered"""


class CapstoneDependencies:
    """Statically infer dependencies out of a straight-line of code"""

    kernel: list[capstone.CsInsn]
    kernel_periodicity: t.Optional[int]  # Is the kernel is periodic (unrolled loop)?
    arch: archinfo.Arch
    only_mem_deps: bool

    _dependencies: set[Dependency]
    _reg_canonicalizer: RegCanonicalizer

    def __init__(
        self,
        kernel: list[capstone.CsInsn],
        arch: archinfo.Arch,
        periodicity: t.Optional[int] = None,
        only_mem_deps: bool = False,
    ):
        self._dependencies = set()
        self.kernel = kernel
        self.kernel_periodicity = periodicity
        self.arch = arch
        self.only_mem_deps = only_mem_deps
        self.CsOperandType = get_cs_operand_type(self.arch)

        self._reg_canonicalizer = RegCanonicalizer(self.arch)

    @classmethod
    def from_angr_extractor(
        cls, angr_extractor: "AngrKernelExtractor", min_insn: int
    ) -> "CapstoneDependencies":
        """Builds an instance from an Angr kernel extractor"""
        kernel, period = angr_extractor.build_unrolled_kernel(min_insn)
        return cls(kernel, angr_extractor.proj.arch, period)

    @classmethod
    def from_elf_reader(
        cls, reader: ElfReader, min_insn: int, only_mem_deps: bool = False
    ) -> "CapstoneDependencies":
        """Builds an instance from a whole ELF file, unrolled (useful with .o)"""
        kernel = reader.kernel
        period = len(reader.kernel)

        required_unroll = ceil(min_insn / period)
        unrolled_kernel: list[capstone.CsInsn] = []
        for _ in range(required_unroll):
            unrolled_kernel += kernel

        return cls(
            unrolled_kernel, reader.archinfo, period, only_mem_deps=only_mem_deps
        )

    @classmethod
    def from_full_elf(
        cls, elf_path: str, min_insn: int, only_mem_deps: bool = False
    ) -> "CapstoneDependencies":
        return cls.from_elf_reader(ElfReader(elf_path), min_insn, only_mem_deps)

    @property
    def unrepeated_kernel(self) -> list[capstone.CsInsn]:
        return self.kernel[: self.kernel_periodicity]

    def get_dependencies(self) -> Dependencies:
        shadow_state = abstract_value.ShadowState(self.arch)
        write_locs = abstract_value.WriteLocs()

        for insn_id, insn in enumerate(self.kernel):
            self._iter_insn(insn, insn_id, shadow_state, write_locs)
        return sorted(list(self._dependencies))

    def _iter_insn(
        self,
        insn: capstone.CsInsn,
        insn_id: int,
        state: abstract_value.ShadowState,
        write_locs: abstract_value.WriteLocs,
    ) -> None:
        # Compute dependencies
        imm_fp = [self.CsOperandType.IMM]
        if hasattr(self.CsOperandType, "FP"):
            imm_fp.append(self.CsOperandType.FP)

        regs_read, regs_written = capstone_hotfix_regs_access(insn)
        for reg in regs_read:
            dst_loc = write_locs.reg_written.get(reg)
            if dst_loc is not None and not self.only_mem_deps:
                self._dependencies.add(Dependency(insn_id, dst_loc))
        for reg in regs_written:
            super_regs = self._reg_canonicalizer.superregs(reg)
            sub_regs = self._reg_canonicalizer.subregs(reg)
            for supreg in super_regs:
                write_locs.reg_written[supreg] = insn_id
                # FIXME: won't detect "compound" registers (eg. write lower part, then
                # upper part, then use whole register). Probably not that critical.
            for subreg in sub_regs:
                write_locs.reg_written[subreg] = insn_id

        for op_id, operand in enumerate(insn.operands):
            typ = self.CsOperandType(operand.type)
            access = capstone_hotfix_access(CsAccess(operand.access), insn, op_id)

            if typ == self.CsOperandType.INVALID:
                raise BadOperand("Bad operand type {} in {}".format(typ.name, insn))

            if CsAccess.READ in access:
                if typ == self.CsOperandType.REG:
                    pass  # Handled above (`regs_read`)
                elif typ in imm_fp:
                    pass  # Nothing to be done
                elif typ == self.CsOperandType.MEM:
                    memloc = state.get_memloc(operand.mem)
                    if memloc in write_locs.mem_written:
                        self._dependencies.add(
                            Dependency(insn_id, write_locs.mem_written[memloc])
                        )
                else:
                    raise BadOperand("Unsupported operand type {}".format(typ.name))
            if CsAccess.WRITE in access:
                if typ == self.CsOperandType.REG:
                    pass  # Handled above (`regs_written`)
                elif typ in imm_fp:
                    raise BadOperand(
                        "Cannot write immediate operand in {}".format(insn)
                    )
                elif typ == self.CsOperandType.MEM:
                    memloc = state.get_memloc(operand.mem)
                    write_locs.mem_written[memloc] = insn_id
                else:
                    raise BadOperand("Unsupported operand type {}".format(typ.name))

        # Update underlying values
        vex_engine = vex_semantics.VexSemanticsEngine.from_capstone_insn(
            insn, self.arch
        )
        try:
            vex_engine.apply(state)
        except (
            vex_semantics.VexSemanticsException,
            abstract_value.RandValException,
        ) as exn:
            logger.error("Semantics: exception occurred while processing %s", insn)
            logger.debug(
                "Instruction has the following VEX semantics:\n%s",
                "\n".join(map(str, vex_engine.irsb.statements)),
            )
            raise exn


def get_periodic_deps(
    deps: Dependencies, kernel_len: int, total_len: int, drop_spurious: bool = False
) -> dict[PeriodicDep, set[int]]:
    """Breaks down dependencies to a set of periodic dependencies, mapped to the set
    of interations at which they miss (not taking into account iterations at which they
    cannot appear, eg. dependency in two iterations earlier on the first iteration).
    :param kernel_len: length of the kernel before unrolling
    :param total_len: length of the full kernel, after unrolling
    :param drop_spurious: if set to True, the dependencies that are not present in at
        least 80% of relevant iterations (floored) are discarded.
    """

    perdeps = collections.defaultdict(set)
    for dep in deps:
        perdep = PeriodicDep.from_dep(dep, kernel_len)
        perdeps[perdep].add(dep.src_pc // kernel_len)

    nb_iters = total_len // kernel_len
    max_missing: int = ceil(nb_iters * 0.2)
    all_iters = set(range(nb_iters))
    missing_deps = {}
    for perdep, occurs in perdeps.items():
        missing = all_iters - occurs
        first_possible_occur = -perdep.at_iter
        missing -= set(range(first_possible_occur))
        if drop_spurious and len(missing) > max_missing:
            continue
        missing_deps[perdep] = missing

    return missing_deps


def analyze_deps(
    deps: Dependencies, dep_analyzer: CapstoneDependencies
) -> dict[PeriodicDep, str]:
    assert dep_analyzer.kernel_periodicity is not None
    perdeps = get_periodic_deps(
        deps, dep_analyzer.kernel_periodicity, len(dep_analyzer.kernel)
    )

    perdeps_analyzed = {}
    for perdep, missing in perdeps.items():
        if not missing:
            perdeps_analyzed[perdep] = "Always"
        else:
            perdeps_analyzed[perdep] = f"Not at iters {missing}"
    return perdeps_analyzed


def analyze_deps_graphviz(
    deps: Dependencies, dep_analyzer: CapstoneDependencies
) -> str:
    """Same as analyze_deps, outputs graphviz format"""
    assert dep_analyzer.kernel_periodicity is not None
    analyze = analyze_deps(deps, dep_analyzer)
    out = []
    out.append("splines=ortho")
    out.append("node[shape=box, group=g1]")
    for insn_id, insn in enumerate(
        dep_analyzer.kernel[: dep_analyzer.kernel_periodicity]
    ):
        out.append(f'insn{insn_id} [label="{insn}"]')

    for insn_id in range(dep_analyzer.kernel_periodicity - 1):
        out.append(f"insn{insn_id} -> insn{insn_id + 1} [style=invis, weight=100]")

    out.append("")

    for dep, descr in analyze.items():
        out.append(f'insn{dep.src} -> insn{dep.dst} [xlabel="<{dep.at_iter}> {descr}"]')

    return "digraph {\n\t" + "\n\t".join(out) + "\n}"


def analyze_deps_txt(deps: Dependencies, dep_analyzer: CapstoneDependencies) -> str:
    """Same as analyze_deps, outputs plaintext human-readable format"""
    analyze = analyze_deps(deps, dep_analyzer)
    out = []
    for dep, descr in analyze.items():
        out.append(
            f"{dep_analyzer.kernel[dep.dst]}\n"
            + f"  <-- {dep_analyzer.kernel[dep.src]}\n"
            + f"  <{dep.at_iter}>\t{descr}"
        )
    return "\n\n".join(out)
