""" Light attempt to make Capstone bindings slightly more pythonic """

import capstone
import collections
import archinfo
import enum
import typing as t
import types
import itertools


VexReg: t.TypeAlias = t.Tuple[archinfo.RegisterOffset, int]


class RegCanonicalizer:
    arch: archinfo.arch.Arch
    _canonical_reg_cache: dict[int, int]
    _subregs_cache: dict[int, list[int]]
    _superregs_cache: dict[int, list[int]]
    _name_to_csreg: dict[str, int]
    _vexreg_to_vexname: dict[VexReg, list[str]]
    _vex_sub_registers: dict[
        VexReg,
        list[VexReg],
    ]
    _vex_super_registers: dict[
        VexReg,
        list[VexReg],
    ]

    def __init__(self, arch: archinfo.arch.Arch):
        self.arch = arch
        self._canonical_reg_cache = {}
        self._subregs_cache = {}
        self._superregs_cache = {}
        csprefix = capstone.CS_ARCH[self.arch.cs_arch][len("CS_ARCH_") :]
        csregprefix = csprefix + "_REG_"
        csmod = getattr(capstone, csprefix.lower())
        csregs = filter(lambda x: x.startswith(csregprefix), dir(csmod))
        self._name_to_csreg = {
            csreg[len(csregprefix) :]: getattr(csmod, csreg) for csreg in csregs
        }

        self._vexreg_to_vexname = collections.defaultdict(list)
        self._make_vexreg_to_vexname()

        self._vex_sub_registers = collections.defaultdict(list)
        self._vex_super_registers = collections.defaultdict(list)
        self._make_related_registers()

    def _make_vexreg_to_vexname(self):
        vex_main_names = set(map(lambda x: x.name, self.arch.register_list))

        def relevance_score(vexname: str):
            score = 0
            if vexname in vex_main_names:
                score += 100  # Should be in front
            if vexname not in self._name_to_csreg:
                score -= 150  # Those not translatable go to the back
            return score

        for name, reg in self.arch.registers.items():
            self._vexreg_to_vexname[reg].append(name)
        for reg in self._vexreg_to_vexname:
            self._vexreg_to_vexname[reg].sort(key=relevance_score, reverse=True)

    def _make_related_registers(self):
        def included(reg1, reg2) -> bool:
            """Whether reg1 <= reg2"""
            return reg1[0] >= reg2[0] and reg1[0] + reg1[1] <= reg2[0] + reg2[1]

        for reg in self.arch.register_list:
            # Inclusions for register itself
            regtuple = (reg.vex_offset, reg.size)
            self._vex_sub_registers[regtuple] = [regtuple]
            self._vex_super_registers[regtuple] = [regtuple]
            for subreg in reg.subregisters:
                subreg_tuple = (
                    archinfo.RegisterOffset(subreg[1] + reg.vex_offset),
                    subreg[2],
                )
                self._vex_sub_registers[regtuple].append(subreg_tuple)
                self._vex_super_registers[subreg_tuple].append(regtuple)

            # All sub-inclusions: quadratic is ok
            for subreg in reg.subregisters:
                subreg_tuple = (
                    archinfo.RegisterOffset(subreg[1] + reg.vex_offset),
                    subreg[2],
                )
                for oth in reg.subregisters:
                    oth_tuple = (
                        archinfo.RegisterOffset(oth[1] + reg.vex_offset),
                        oth[2],
                    )
                    if included(oth_tuple, subreg_tuple):
                        self._vex_sub_registers[subreg_tuple].append(oth_tuple)
                        self._vex_super_registers[oth_tuple].append(subreg_tuple)

    def cs_name(self, reg: int) -> t.Optional[str]:
        """Returns the name of the register according to capstone constants"""
        return self.arch.capstone.reg_name(reg)

    def cs_to_vex(self, csreg: int) -> t.Optional[VexReg]:
        """Convert a Capstone register ID to a VEX register offset"""
        csname = self.cs_name(csreg)
        if csname is None:
            return None
        vexname = csname.lower()  # heh, works more often than not.
        return self.arch.registers.get(vexname, None)

    def vexname_to_cs(self, vex_name: str) -> t.Optional[int]:
        return self._name_to_csreg.get(vex_name.upper(), None)

    def vex_to_cs(self, vexreg: VexReg) -> t.Optional[int]:
        try:
            vex_name = self._vexreg_to_vexname[vexreg][0]
        except KeyError:
            return None

        # Map back to cs register
        return self.vexname_to_cs(vex_name)

    def vex_to_cs_list(self, vexreg: VexReg) -> list[int]:
        vex_names = self._vexreg_to_vexname[vexreg]  # No keyerror: defaultdict
        return list(filter(None, map(self.vexname_to_cs, vex_names)))

    def canonical(self, reg: int) -> int:
        """Returns the id of the canonical register for this register"""
        if reg not in self._canonical_reg_cache:
            self._canonical_reg_cache[reg] = self._get_canonical(reg) or reg
        return self._canonical_reg_cache[reg]

    def _get_canonical(self, reg: int) -> t.Optional[int]:
        vex_reg = self.cs_to_vex(reg)
        if not vex_reg:
            return None
        vex_base: t.Optional[VexReg] = self.arch.subregister_map.get(vex_reg, None)
        if vex_base is None:
            return None
        return self.vex_to_cs(vex_base)

    def _get_reglist(self, reg: int, lookup: dict[VexReg, list[VexReg]]) -> list[int]:
        vex_reg = self.cs_to_vex(reg)
        if not vex_reg:
            return [reg]
        vex_matches = lookup[vex_reg]
        if not vex_matches:
            return [reg]
        cs_matches = list(
            itertools.chain.from_iterable(
                filter(None, map(self.vex_to_cs_list, vex_matches))
            )
        )
        if not cs_matches:
            return [reg]
        if reg not in cs_matches:
            cs_matches.append(reg)
        return cs_matches

    def subregs(self, reg: int) -> list[int]:
        """Returns the list of CS registers included in the provided CS register
        (itself included)"""
        if reg not in self._subregs_cache:
            self._subregs_cache[reg] = self._get_subregs(reg)
        return self._subregs_cache[reg]

    def _get_subregs(self, reg: int) -> list[int]:
        return self._get_reglist(reg, self._vex_sub_registers)

    def superregs(self, reg: int) -> list[int]:
        """Returns the list of registers whose memory area is a superset of this
        register (itself included)"""
        if reg not in self._superregs_cache:
            self._superregs_cache[reg] = self._get_superregs(reg)
        return self._superregs_cache[reg]

    def _get_superregs(self, reg: int) -> list[int]:
        return self._get_reglist(reg, self._vex_super_registers)


class BadPrefix(Exception):
    """Raised when an argument string is expected to start with a prefix, but does
    not"""


def _make_cs_dict(module, prefix: str) -> dict[int, str]:
    """Create a dictionary similar to eg. capstone.CS_OP with all the constants from
    :module: with a name starting with :prefix:."""
    return {
        getattr(module, key): key
        for key in filter(lambda x: x.startswith(prefix), dir(module))
    }


def _make_cs_enum(
    name: str,
    elements: dict[int, str],
    flags: bool = False,
    prefix: t.Optional[str] = None,
) -> type[enum.Enum]:
    """Create an IntEnum from a dictionary of capstone labels"""

    def strip_prefix(key: str):
        if not prefix:
            return key
        if not key.startswith(prefix):
            raise BadPrefix(f"Enum element {key} does not start with {prefix}.")
        return key[len(prefix) :]

    revdict = {
        strip_prefix(v): k for k, v in elements.items()
    }  # By construction values are unique
    if flags:
        return enum.IntFlag(name, revdict)  # type: ignore  # metaclass magic
    return enum.IntEnum(name, revdict)  # type: ignore  # metaclass magic


def get_cs_operand_type(arch: archinfo.Arch) -> t.Any:
    if isinstance(arch, (archinfo.ArchAMD64, archinfo.ArchX86)):
        x86_optyp = _make_cs_dict(capstone.x86, "X86_OP_")
        return _make_cs_enum("CsOperandType", x86_optyp, prefix="X86_OP_")
    return _make_cs_enum("CsOperandType", capstone.CS_OP, prefix="CS_OP_")


CsAccess: t.Any = _make_cs_enum("CsAccess", capstone.CS_AC, flags=True, prefix="CS_AC_")
X86_REG = _make_cs_dict(capstone.x86, "X86_REG_")
CsX86Reg: t.Any = _make_cs_enum("CsX86Reg", X86_REG, prefix="X86_REG_")


def capstone_hotfix_access(
    access: CsAccess, insn: capstone.CsInsn, op_id: int
) -> CsAccess:
    """Fix major errors in accesses"""
    if insn._cs.arch == capstone.CS_ARCH_ARM64:
        # CMP x,y actually is SUBS Xzr,x,y, so capstone gets confused
        if insn.mnemonic == "cmp":
            return CsAccess.READ
        if insn.mnemonic == "movi" and op_id == 0:
            return CsAccess.WRITE

    return access


def capstone_hotfix_regs_access(insn: capstone.CsInsn) -> tuple[list[int], list[int]]:
    """Fix major errors in accesses"""
    regs_read, regs_written = insn.regs_access()
    regs_read = list(regs_read)
    regs_written = list(regs_written)

    if insn._cs.arch == capstone.CS_ARCH_ARM64:
        # CMP x,y actually is SUBS Xzr,x,y, so capstone gets confused
        if insn.mnemonic == "cmp":
            if insn.operands[0].type == capstone.CS_OP_REG:
                op0_reg = insn.operands[0].reg
                if op0_reg not in regs_read:
                    regs_read.append(op0_reg)
                try:
                    regs_written.remove(op0_reg)
                except ValueError:
                    pass
        if insn.mnemonic == "movi":
            if insn.operands[0].type == capstone.CS_OP_REG:
                op0_reg = insn.operands[0].reg
                if op0_reg in regs_read:
                    regs_read.remove(op0_reg)
    return (regs_read, regs_written)
