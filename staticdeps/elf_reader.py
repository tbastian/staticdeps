import typing as t

import archinfo
import capstone
from elftools.elf import elffile


ARCHINFO_FROM_ARCHSTR: dict[str, archinfo.Arch] = {
    "x86": archinfo.ArchX86(),
    "x64": archinfo.ArchAMD64(),
    "ARM": archinfo.ArchARM(),
    "AArch64": archinfo.ArchAArch64(),
}


class ElfReader:
    """Reads a whole ELF file"""

    path: str
    raw_bytes: bytes
    kernel: list[capstone.CsInsn]
    archinfo: archinfo.Arch

    _cs: capstone.Cs

    def __init__(
        self,
        path: str,
        with_bytes: t.Optional[bytes] = None,
        offset: t.Optional[int] = None,
    ):
        if offset is None:
            offset = 0
        self.path = path

        with open(self.path, "rb") as handle:
            elf = elffile.ELFFile(handle)
            if with_bytes is not None:
                self.raw_bytes = with_bytes
            else:
                self.raw_bytes = elf.get_section_by_name(".text").data()
            self.archinfo = ARCHINFO_FROM_ARCHSTR[elf.get_machine_arch()]

        self._cs = self.archinfo.capstone
        self._cs.detail = True
        self._cs.syntax = capstone.CS_OPT_SYNTAX_ATT

        self.kernel = list(self._cs.disasm(self.raw_bytes, offset))
