""" Entrypoints """

import logging
import argparse
from pathlib import Path
import typing as t
from staticdeps import angr_deps, capstone_deps


logger = logging.getLogger(__name__)


def kernel_args(parser, min_insn: bool = True):
    parser.add_argument("binary", help="Path to the binary file", type=Path)
    parser.add_argument("symbol", help="Symbol to analyse in the file")
    if min_insn:
        parser.add_argument(
            "-n",
            "--min-insn",
            type=int,
            default=100,
            help="How much the kernel should be unrolled.",
        )


def logging_args(parser):
    """Add logging arguments to a parser"""
    parser.add_argument(
        "-v",
        "--verbose",
        dest="loglevel",
        action="store_const",
        default=logging.WARNING,
        const=logging.INFO,
        help="Verbose output (loglevel info)",
    )
    parser.add_argument(
        "-g",
        "--debug",
        dest="loglevel",
        action="store_const",
        default=logging.WARNING,
        const=logging.DEBUG,
        help="More verbose output (loglevel debug)",
    )


def apply_logging_args(args):
    logging.getLogger(None).setLevel(args.loglevel)


def export_kernel():
    parser = argparse.ArgumentParser(
        "export kernel",
        description=("Extracts a kernel from an ELF file and exports it."),
    )
    logging_args(parser)
    kernel_args(parser, min_insn=False)
    parser.add_argument(
        "--keep-jump",
        help="Do not remove the trailing jump of the kernel",
        action="store_true",
    )
    parser.add_argument(
        "--dump", action="store_true", help="Print the kernel in a human-readable form"
    )
    parser.add_argument(
        "--export-raw",
        help="Path in which the kernel will be stored",
        type=Path,
    )
    parser.add_argument(
        "--tb",
        help="Export as a tb file (support for Palmed benchmarking)",
        action="store_true",
    )

    args = parser.parse_args()
    apply_logging_args(args)

    kernel_extractor = angr_deps.AngrKernelExtractor(args.binary, args.symbol)

    if args.dump:
        ker = kernel_extractor.extract_kernel(args.keep_jump)
        print("------ KERNEL ------")
        for insn in ker:
            op_bytes = " ".join(map(lambda byte: f"{byte:02x}", insn.bytes))
            print(f"{insn.address:08x}:\t{op_bytes:<17}\t{insn.mnemonic} {insn.op_str}")
        print("---- END KERNEL ----")

    if args.tb:
        ker = kernel_extractor.extract_kernel(args.keep_jump)
        print(f"TB 0x{ker[0].address:08x} was executed 1 time(s)")
        for insn in ker:
            op_bytes = " ".join(map(lambda byte: f"{byte:02x}", insn.bytes))
            print(f"{op_bytes}\t\t[[{insn.mnemonic} {insn.op_str}]]")
        print("End of TB")

    if args.export_raw:
        with args.export_raw.open("wb") as handle:
            kernel_extractor.export_raw_kernel(handle, keep_jump=args.keep_jump)


def test_kernel():
    parser = argparse.ArgumentParser(
        "test_kernel",
        description=(
            "Load a kernel and compute its deps, then throw everything away -- "
            "just to see whether it works for this binary."
        ),
    )
    logging_args(parser)
    kernel_args(parser, min_insn=True)
    parser.add_argument("--iaca-check", action="store_true")

    args = parser.parse_args()
    apply_logging_args(args)

    kernel_extractor = angr_deps.AngrKernelExtractor(args.binary, args.symbol)
    if args.iaca_check:
        if not kernel_extractor.check_with_iaca_marks():
            print("[FAIL] did not match IACA marks expectations")
    cs_deps = capstone_deps.CapstoneDependencies.from_angr_extractor(
        kernel_extractor, args.min_insn
    )
    _ = cs_deps.get_dependencies()
