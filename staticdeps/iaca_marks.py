import typing as t
import logging
import capstone
import archinfo
import enum

logger = logging.getLogger(__name__)


class MarksNotFound(Exception):
    """Raised when IACA marks are not found"""


IACA_MARK_INSNS: int = 2


class IacaMark(enum.IntEnum):
    """IACA marker values"""

    IACA_START = 111
    IACA_END = 222


def is_iaca_mark(insns: list[capstone.CsInsn], pos: int, mark: IacaMark) -> bool:
    """Check whether an instruction is a IACA mark of the provided type"""
    marker_bytes = bytes([0xBB, int(mark), 0, 0, 0])
    marker_nop = bytes([0x64, 0x67, 0x90])
    if not pos + 1 < len(insns):
        return False
    return insns[pos].bytes == marker_bytes and insns[pos + 1].bytes == marker_nop


def find_iaca_marks(insns: list[capstone.CsInsn]) -> t.Optional[tuple[int, int]]:
    """Find iaca start and end marks in the provided block"""

    start_mark = None
    for pos, _ in enumerate(insns):
        if is_iaca_mark(insns, pos, IacaMark.IACA_START):
            start_mark = pos
            break
    else:
        return None
    for pos in range(start_mark + 2, len(insns)):
        if is_iaca_mark(insns, pos, IacaMark.IACA_END):
            return (start_mark, pos)
    return None


def find_iaca_marks_or_fail(insns: list[capstone.CsInsn]) -> tuple[int, int]:
    marks = find_iaca_marks(insns)
    if marks is None:
        raise MarksNotFound(insns)
    return marks
