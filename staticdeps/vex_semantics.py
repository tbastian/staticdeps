import typing as t
import re
import enum
import operator
import logging
from collections import defaultdict
import functools
import archinfo
import capstone
import pyvex
from staticdeps.abstract_value import random_values as abstract_value

logger = logging.getLogger(__name__)


class VexSemanticsException(Exception):
    """Base vex_semantics exception"""


class UnsupportedVex(VexSemanticsException):
    """Some VEX IR that wasn't accounted for"""


class BadVex(VexSemanticsException):
    """Some VEX IR that is invalid in this context, up to our understanding"""


VEX_OP_RE = re.compile(
    r"Iop_(?P<oper>[a-zA-Z]+?)(?P<float>F?)(?P<bitsize>[0-9]+)(?P<typsign>[SUF]?)(?:(?P<lane>[0-9])?(?P<simd>x[0-9]+))?"
)


class TypeSign(enum.Enum):
    SIGNED = enum.auto()
    UNSIGNED = enum.auto()
    FLOAT = enum.auto()

    @classmethod
    def of_ident(cls, ident: str) -> t.Optional["TypeSign"]:
        match ident:
            case "S":
                return cls.SIGNED
            case "U":
                return cls.UNSIGNED
            case "F":
                return cls.FLOAT
        return None


class OperatorInfo(t.NamedTuple):
    oper: str
    floatop: bool
    bitsize: int
    typesign: t.Optional[TypeSign]
    lane: t.Optional[int]
    simd_size: t.Optional[int]

    def __str__(self):
        return f'{self.oper} [{"float" if self.floatop else ""}{self.bitsize}] typ {self.typesign or "?"} x{self.simd_size or 1}[{self.lane or "*"}]'

    @property
    def is_simd(self) -> bool:
        return self.simd_size is not None

    @property
    def is_float(self) -> bool:
        """Whether this operator is a float operator (simd or not)"""
        return self.floatop or self.typesign == TypeSign.FLOAT

    @classmethod
    def from_opstr(cls, opstr: str) -> t.Optional["OperatorInfo"]:
        """Match from the VEX operator string, or return None if the operator does not
        match"""

        def int_if(x: t.Optional[str]) -> t.Optional[int]:
            if x is not None:
                return int(x)
            return None

        match = VEX_OP_RE.fullmatch(opstr)
        if not match:
            return None
        matchdict = match.groupdict()
        simd_sz = int(matchdict["simd"][1:]) if matchdict["simd"] else None
        return cls(
            oper=matchdict["oper"],
            floatop=bool(matchdict["float"]),
            bitsize=int(matchdict["bitsize"]),
            typesign=TypeSign.of_ident(matchdict["typsign"]),
            lane=int_if(matchdict["lane"]),
            simd_size=simd_sz,
        )


def vex_typesize(typ: str, default: int = 64) -> int:
    """Get the number of bits for the given type"""
    assert typ.startswith("Ity_")
    if not typ.startswith("Ity_I"):
        return default
    return int(typ[len("Ity_I") :])


def vex_simd_opsize(operator: str) -> t.Optional[int]:
    """Returns the SIMD size of the given VEX operator, or None if this does not
    appear to be a SIMD operation"""
    op_match = OperatorInfo.from_opstr(operator)
    if not op_match:
        return None
    return op_match.simd_size


class VexSemanticsEngine:
    """Follows VEX semantics and applies them to a ShadowState"""

    temps: dict[archinfo.TmpVar, abstract_value.StoredValue]
    irsb: pyvex.block.IRSB

    VEX_OP_RE_INTCONV = re.compile(
        r"Iop_(?P<from>[0-9]+)(?P<fromqual>[US]?)to(?P<to>[0-9]+)"
    )
    VEX_OP_RE_VECINTCONV = re.compile(
        r"Iop_V(?P<from>[0-9]+)(?P<fromqual>(HI)?)to(?P<to>[0-9]+)"
    )
    VEX_OP_RE_INTVECCONV = re.compile(
        r"Iop_(?P<from>[0-9]+)(?P<fromqual>[US]?)(?P<frommul>x[0-9]+)?to(?P<to>V[0-9]+)"
    )

    def __init__(self, irsb: pyvex.block.IRSB):
        self.temps = {}
        self.irsb = irsb

    @classmethod
    def from_capstone_insn(cls, insn: capstone.CsInsn, arch: archinfo.Arch):
        """Create a semantics engine by lifting a capstone instruction to an irsb"""
        irsb = pyvex.lift(insn.bytes, insn.address, arch)
        return cls(irsb)

    @classmethod
    def from_capstone_insns(cls, insns: list[capstone.CsInsn], arch: archinfo.Arch):
        """Create a semantics engine by lifting a list of capstone instructions to an irsb"""
        op_bytes = functools.reduce(operator.add, map(lambda x: x.bytes, insns))
        irsb = pyvex.lift(op_bytes, insns[0].address, arch)
        return cls(irsb)

    def vector_sizes(self) -> dict[int, int]:
        """Map the various SIMD sizes that appear in operations to their occurrence
        count, according to VEX engine"""
        occur: dict[int, int] = defaultdict(int)

        for oper in self.irsb.operations:
            opsize = vex_simd_opsize(oper)
            if opsize is not None:
                occur[opsize] += 1
        return occur

    def largest_vector_size(self) -> int:
        """Return the largest SIMD size used in this block"""
        vec_sizes = self.vector_sizes()
        vec_sizes[1] = 1  # Default value
        return max(vec_sizes.keys())

    def apply(self, state: abstract_value.ShadowState):
        """Iterate over the statements from the IRSB"""
        for stmt in self.irsb.statements:
            self._iter_stmt(stmt, state)

    def _iter_stmt(self, stmt: pyvex.stmt.IRStmt, state: abstract_value.ShadowState):
        match stmt:
            case pyvex.stmt.IMark():
                pass
            case pyvex.stmt.NoOp():
                pass
            case pyvex.stmt.AbiHint():
                pass
            case pyvex.stmt.MBE():
                pass
            case pyvex.stmt.Exit():
                pass
            case pyvex.stmt.Dirty():
                raise BadVex("Dirty stmt should only appear in instrumented code")
            case pyvex.stmt.Put() as put:
                val = self._compute_vex_expr(put.data, state)
                if val.debug:
                    logger.debug("Put: register %d is now %s", put.offset, val)
                state.regs[put.offset] = val
            case pyvex.stmt.PutI() as put:
                logger.warning("Unsupported PutI: %s", put)
            case pyvex.stmt.WrTmp() as wr_tmp:
                self.temps[wr_tmp.tmp] = self._compute_vex_expr(wr_tmp.data, state)
            case pyvex.stmt.Store() as store:
                val = self._compute_vex_expr(store.data, state)
                addr = self._compute_vex_expr(store.addr, state)
                if addr.valid:
                    state.memory[addr.value] = val
                else:
                    logger.warning("Invalid address for Store: %s", store)
            case pyvex.stmt.StoreG() as store:
                logger.warning("Unsupported StoreG: %s", store)
            case pyvex.stmt.CAS() as cas:
                logger.warning("Unsupported CAS: %s", cas)
            case pyvex.stmt.LLSC() as llsc:
                logger.warning("Unsupported LLSC: %s", llsc)
            case pyvex.stmt.LoadG() as load:
                logger.warning("Unsupported LoadG: %s", load)
            case _:
                raise UnsupportedVex(
                    f"Unsupported statement type: {stmt.__class__.__name__}"
                )

    def _compute_vex_expr(
        self,
        expr: pyvex.expr.IRExpr,
        state: abstract_value.ShadowState,
    ) -> abstract_value.StoredValue:
        """Compute the value, in the current context, of a VEX expression, or return
        an invalid value if unfeasable or out of scope"""

        match expr:
            case pyvex.expr.Binder():
                raise BadVex("Binder expr shouldn't be seen outside of VEX's guts")
            case pyvex.expr.VECRET():
                raise BadVex(
                    "VECRET expr are only arguments for unsupported Dirty calls"
                )
            case pyvex.expr.GSPTR():
                raise BadVex(
                    "GSPTR expr are only arguments for unsupported Dirty calls"
                )
            case pyvex.expr.CCall() as ccall:
                if not str(ccall).startswith("amd64g_"):
                    logger.warning("Unsupported CCall: %s", ccall)
                return abstract_value.StoredValue.invalid()
            case pyvex.expr.GetI() as get:
                logger.warning("Unsupported GetI: %s", get)
                return abstract_value.StoredValue.invalid()
            case pyvex.expr.RdTmp() as rdtmp:
                return self.temps.get(rdtmp.tmp, abstract_value.StoredValue.invalid())
            case pyvex.expr.Get() as get:
                val = state.regs[get.offset]
                if val.debug:
                    logger.debug("Get: register %d is %s", get.offset, val)
                return val.cast(vex_typesize(get.type))
            case pyvex.expr.Qop() as oper:
                return self._compute_vex_op(expr, oper.op, oper.args, state)
            case pyvex.expr.Triop() as oper:
                return self._compute_vex_op(expr, oper.op, oper.args, state)
            case pyvex.expr.Binop() as oper:
                return self._compute_vex_op(expr, oper.op, oper.args, state)
            case pyvex.expr.Unop() as oper:
                return self._compute_vex_op(expr, oper.op, oper.args, state)
            case pyvex.expr.Load() as load:
                addr = self._compute_vex_expr(load.addr, state)
                if addr.valid:
                    return state.memory[addr.value]
                return abstract_value.StoredValue.invalid()
            case pyvex.expr.Const() as cst:
                return abstract_value.StoredValue(cst.con.value, accurate=True)
            case pyvex.expr.ITE() as ite:
                cond = self._compute_vex_expr(ite.cond, state)
                if not (cond.valid and cond.accurate):
                    # Cannot hope to do if-then-else with a random value here
                    return abstract_value.StoredValue.invalid()
                if cond == 0:
                    return self._compute_vex_expr(ite.iffalse, state)
                return self._compute_vex_expr(ite.iftrue, state)
            case _:
                raise UnsupportedVex(
                    f"Unsupported expression type: {expr.__class__.__name__}"
                )
        return abstract_value.StoredValue.invalid()

    def _compute_vex_op(
        self,
        expr: pyvex.expr.IRExpr,
        oper: str,
        operands: list[pyvex.expr.IRExpr],
        state: abstract_value.ShadowState,
    ) -> abstract_value.StoredValue:
        """Apply an operator (unary, binary, ternary, quadrary) to its operands and
        return the resulting value."""

        def compute_operand(operand: pyvex.expr.IRExpr) -> abstract_value.StoredValue:
            return self._compute_vex_expr(operand, state)

        def apply(oper_func, args) -> abstract_value.StoredValue:
            arg_vals = map(compute_operand, args)
            return oper_func(*arg_vals)

        if oper == "Iop_INVALID":
            raise BadVex(f"Encountered invalid operator in {expr}")

        # See VEX/pub/libvex_ir.h in valgrind for a full list of operands.

        # Try "standard operators": Iop_<opname><opbitsize>, eg Iop_Add32
        stdop = OperatorInfo.from_opstr(oper)
        if stdop:
            opstr_to_opfunc = {
                "Add": operator.add,
                "Sub": operator.sub,
                "Mul": operator.mul,
                "Shl": operator.lshift,
                "Shr": operator.rshift,
                "Sar": lambda a, b: a.rshift_arith(b),
                "Or": operator.or_,
                "And": operator.and_,
            }
            if stdop.is_simd:
                # Ignore silently SIMD operations -- those are not addresses
                logger.debug("Silently ignoring unsupported SIMD operator %s", oper)
                return abstract_value.StoredValue.invalid()
            if stdop.is_float:
                # Ignore silently float operations -- those are not addresses
                logger.debug("Silently ignoring unsupported float operator %s", oper)
                return abstract_value.StoredValue.invalid()
            if stdop.oper in opstr_to_opfunc:
                try:
                    val = apply(opstr_to_opfunc[stdop.oper], operands)
                except TypeError as exn:
                    raise UnsupportedVex(
                        f"Unsupported seemingly-standard operator <{oper}> "
                        + f"encountered in expr {expr}"
                    ) from exn
                bitsz = int(stdop.bitsize or 64)
                signed = stdop.typesign == TypeSign.SIGNED
                return val.cast(bitsz, signed=signed)

        # Check whether this is a conversion
        intconv_match = self.__class__.VEX_OP_RE_INTCONV.match(oper)
        if intconv_match:
            assert len(operands) == 1
            src = int(intconv_match.group("from"))
            dst = int(intconv_match.group("to"))
            fromqual = intconv_match.group("fromqual")
            signed = fromqual == "S"

            srcval = compute_operand(operands[0])
            return srcval.cast(dst, fromsize=src, signed=signed)

        if oper.startswith("Iop_Reinterp"):
            # Reinterp operations all imply floats or fixed-point decimals -- ignore
            logger.debug("Silently ignoring unsupported float operator %s", oper)
            return abstract_value.StoredValue.invalid()

        if oper in ("Iop_64HLtoV128", "Iop_64UtoV128"):
            return compute_operand(operands[0])  # We only have 64 bits anyway

        vecintconv_match = self.__class__.VEX_OP_RE_VECINTCONV.match(oper)
        if vecintconv_match:
            assert len(operands) == 1
            if vecintconv_match.group("fromqual") == "HI":
                return abstract_value.StoredValue.fresh()  # We don't have high bits
            srcval = compute_operand(operands[0])
            dst = int(vecintconv_match.group("to"))
            return srcval.cast(dst)

        intvecconv_match = self.__class__.VEX_OP_RE_INTVECCONV.match(oper)
        if intvecconv_match:
            # Ignore
            return abstract_value.StoredValue.invalid()

        # Rules to arbitrarily silence some operators and return invalid instead
        if oper.startswith("Iop_Div"):  # no divisions on an address
            return abstract_value.StoredValue.invalid()
        if "V128" in oper and any(x in oper for x in ("And", "Or", "Not", "Xor")):
            # No bitwise vector operations on addresses, I expect.
            return abstract_value.StoredValue.invalid()

        logger.warning("Unsupported operator <%s> encountered in expr %s", oper, expr)
        return abstract_value.StoredValue.invalid()
