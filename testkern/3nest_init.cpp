#include <stdlib.h>

const int SIZE = 2048;
const int OUTSIZE = 128;
const int REDUCTION = SIZE / OUTSIZE;

volatile int data[SIZE][SIZE];
volatile int out[OUTSIZE][OUTSIZE];

int main(void) {
    for(int i=0; i < OUTSIZE; ++i) {
        for(int j=0; j < OUTSIZE; ++j)
            out[i][j] = 0;
        for(int j=0; j < OUTSIZE; ++j) {
            for(int k=0; k < REDUCTION; ++k) {
                out[i][j] += data[i*REDUCTION+k][j*REDUCTION+k];
            }
        }
    }
    return 0;
}
