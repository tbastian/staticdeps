#include <stdlib.h>

const int SIZE = 2048;

volatile int data[SIZE];

int main(void) {
    data[0] = 0;
    data[1] = 1;
    for(int i=2; i < SIZE; ++i) {
        data[i] = data[i-1] + data[i-2];
    }
    return 0;
}
