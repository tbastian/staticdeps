import typing as t
import pytest

from staticdeps import angr_deps


class MockGraph:
    succs: dict[int, list[int]]
    nodes: dict[int, "_MockBlk"]

    class _MockBlk:
        graph: "MockGraph"
        blkid: int

        def __init__(self, blkid: int, graph: "MockGraph"):
            self.graph = graph
            self.blkid = blkid
            assert self.blkid in self.graph.succs

        def successors(self) -> t.Iterable["MockGraph._MockBlk"]:
            for succ in self.graph.succs[self.blkid]:
                yield self.graph.node(succ)

        def __str__(self):
            return f"<{self.__class__.__name__}: {self.blkid}>"

        def __repr__(self):
            return f"{self.__class__.__name__}({self.blkid})"

        def __eq__(self, other):
            return self.blkid == other.blkid

        def __hash__(self):
            return self.blkid

        @property
        def addr(self):
            return self.blkid

    def __init__(self, succs: dict[int, list[int]]):
        for blk_succs in succs.values():
            for succ in blk_succs:
                assert succ in succs
        self.succs = succs.copy()
        self.nodes = {}

    def node(self, nid: int) -> _MockBlk:
        if nid not in self.nodes:
            self.nodes[nid] = MockGraph._MockBlk(nid, self)
        return self.nodes[nid]

    def blk_set(self):
        return [self.node(key) for key in self.succs]


def test__TarjanSCC():
    loopnest = MockGraph(
        {
            0: [4, 5],
            1: [4, 5],
            2: [4, 5],
            3: [4, 5],
            4: [4, 5],
            5: [3, 6],
            6: [2, 7],
            7: [1, 8],
            8: [],
        }
    )

    assert angr_deps._TarjanSCC(loopnest.blk_set())() == [
        loopnest.node(0),
        [
            loopnest.node(1),
            [
                loopnest.node(2),
                [loopnest.node(3), [loopnest.node(4)], loopnest.node(5)],
                loopnest.node(6),
            ],
            loopnest.node(7),
        ],
        loopnest.node(8),
    ]

    withinit = MockGraph(
        {
            0: [2, 3],
            1: [2, 3],
            2: [2, 3],
            3: [5, 6],
            4: [5, 6],
            5: [5, 6],
            6: [4, 7],
            7: [1, 8],
            8: [],
        }
    )
    assert angr_deps._TarjanSCC(withinit.blk_set())() == [
        withinit.node(0),
        [
            withinit.node(1),
            [
                withinit.node(2),
            ],
            withinit.node(3),
            [withinit.node(4), [withinit.node(5)], withinit.node(6)],
            withinit.node(7),
        ],
        withinit.node(8),
    ]
