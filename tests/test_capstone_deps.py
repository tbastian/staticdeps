import typing as t
import archinfo
import capstone as cs
from staticdeps import capstone_deps
from staticdeps.abstract_deps import Dependency, Dependencies, PeriodicDep


def _get_deps(
    arch: archinfo.Arch, code: bytearray
) -> tuple[Dependencies, capstone_deps.CapstoneDependencies]:
    """Extract the dependencies from an asm snippet"""
    cs_engine = cs.Cs(arch.cs_arch, arch.cs_mode)
    cs_engine.detail = True
    insn = list(cs_engine.disasm(code, 0))
    cs_deps = capstone_deps.CapstoneDependencies(insn, arch)
    return cs_deps.get_dependencies(), cs_deps


def ensure_deps(arch: archinfo.Arch, code: bytearray, deps: Dependencies):
    """Check that the dependencies for `code` match the expected provided deps."""
    exp_depset = set(deps)
    found_deps, _ = _get_deps(arch, code)
    found_depset = set(found_deps)
    assert exp_depset == found_depset


def ensure_periodic_deps(
    arch: archinfo.Arch, code: bytearray, reps: int, deps: list[PeriodicDep]
):
    """Check that the periodic dependencies for `code`, when repeated `reps` time,
    match the expected provided deps."""
    unrolled_code = code * reps
    exp_depset = set(deps)
    found_deps, analyzer = _get_deps(arch, unrolled_code)
    kernel_size = len(analyzer.kernel) // reps
    found_perdeps = capstone_deps.get_periodic_deps(
        found_deps, kernel_size, len(analyzer.kernel)
    )
    assert set(found_perdeps.keys()) == exp_depset and not any(found_perdeps.values())


def hexasm(*asm: t.Tuple[str, str]) -> bytearray:
    """Transform (vaguely) human-readable asm of the form
    `list[(hex str, assembly str)]` into a bytearray of instruction bytes"""
    return bytearray.fromhex(" ".join(map(lambda x: x[0], asm)))


def test_cs_deps_amd64():
    """Test on x86"""
    arch = archinfo.ArchAMD64()

    ensure_deps(  # Directly through register
        arch,
        hexasm(
            ("83 c0 01", "add    $0x1,%eax"),
            ("31 c0", "xor    %eax,%eax"),
        ),
        [Dependency(1, 0)],
    )
    ensure_deps(  # Through sub-register
        arch,
        hexasm(
            ("83 c0 01", "add    $0x1,%eax"),
            ("50", "push   %rax"),
        ),
        [Dependency(1, 0)],
    )
    ensure_deps(  # Through super register
        arch,
        hexasm(
            ("66 b8 01 00", "mov    $0x1,%ax"),
            ("48 83 c0 02", "add    $0x2,%rax"),
        ),
        [Dependency(1, 0)],
    )
    ensure_deps(  # No deps at all
        arch,
        hexasm(
            ("48 01 c6", "add    %rax,%rsi"),
            ("54", "push   %rsp"),
        ),
        [],
    )
    ensure_deps(  # Through memory, straightforward
        arch,
        hexasm(
            ("4c 89 00", "mov    %r8,(%rax)"),
            ("4c 8b 08", "mov    (%rax),%r9"),
        ),
        [Dependency(1, 0)],
    )
    ensure_deps(  # Through memory, arithmetic
        arch,
        hexasm(
            ("48 89 c3", "mov    %rax,%rbx"),
            ("48 83 c0 2a", "add    $0x2a,%rax"),
            ("48 83 c3 2a", "add    $0x2a,%rbx"),
            ("48 c7 00 0c 00 00 00", "movq   $0xc,(%rax)"),
            ("48 8b 0b", "mov    (%rbx),%rcx"),
        ),
        map(lambda x: Dependency(*x), [(2, 0), (3, 1), (4, 2), (4, 3)]),
    )
    ensure_deps(  # Through memory, more arithmetic
        arch,
        hexasm(
            ("48 89 c3", "mov    %rax,%rbx"),
            ("49 c7 c0 01 00 00 00", "mov    $0x1,%r8"),
            ("49 c1 e0 02", "shl    $0x2,%r8"),
            ("49 83 c0 02", "add    $0x2,%r8"),
            ("4d 6b c0 07", "imul   $0x7,%r8,%r8"),
            ("4c 01 c0", "add    %r8,%rax"),
            ("49 c7 c0 14 00 00 00", "mov    $0x14,%r8"),
            ("4a 8d 5c 43 02", "lea    0x2(%rbx,%r8,2),%rbx"),
            ("48 c7 00 0c 00 00 00", "movq   $0xc,(%rax)"),
            ("48 8b 0b", "mov    (%rbx),%rcx"),
        ),
        map(
            lambda x: Dependency(*x),
            [(2, 1), (3, 2), (4, 3), (5, 4), (7, 6), (7, 0), (8, 5), (9, 7), (9, 8)],
        ),
    )
    ensure_periodic_deps(  # Periodic
        arch,
        hexasm(
            ("48 8b 04 17", "mov    (%rdi,%rdx,1),%rax"),
            ("48 89 d6", "mov    %rdx,%rsi"),
            ("48 83 ee 04", "sub    $0x4,%rsi"),
            ("48 03 04 37", "add    (%rdi,%rsi,1),%rax"),
            ("48 83 c2 04", "add    $0x4,%rdx"),
            ("48 89 04 17", "mov    %rax,(%rdi,%rdx,1)"),
        ),
        20,
        [
            PeriodicDep(2, 1, 0),
            PeriodicDep(3, 0, 0),
            PeriodicDep(3, 2, 0),
            PeriodicDep(5, 4, 0),
            PeriodicDep(5, 3, 0),
            PeriodicDep(0, 5, -1),
            PeriodicDep(3, 5, -2),
            PeriodicDep(1, 4, -1),
            PeriodicDep(0, 4, -1),
            PeriodicDep(4, 4, -1),
        ],
    )
