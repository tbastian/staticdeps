from staticdeps.abstract_value.random_values import StoredValue
from staticdeps.abstract_value import random_values
import numpy as np
import pytest
import warnings


def test_StoredValue():
    # Might spuriously fail once in a few lifetimes
    assert StoredValue.fresh() != StoredValue.fresh()

    v1 = StoredValue.fresh()
    v2 = StoredValue.fresh()
    v_low = StoredValue(2)
    v_noMSB = StoredValue.fresh() >> 1
    v_overflow = StoredValue((1 << 64) - 1)
    with warnings.catch_warnings():  # Ignore overflows
        warnings.simplefilter("ignore", category=RuntimeWarning)

        assert not v1.accurate
        assert not v2.accurate
        assert (v1 + 12).value == (v1.value + 12) % StoredValue.MODULUS
        assert not (v1 + 12).accurate
        assert (v1 + v2).value == (v1.value + v2.value) % StoredValue.MODULUS
        assert (v1 << 2).value == (v1.value << np.uint16(2)) % StoredValue.MODULUS
        assert (v1 >> 1).value == (v1.value >> np.uint16(1)) % StoredValue.MODULUS
        assert (v_overflow >> 2) & (1 << 63) == 0  # rshift is logical
        assert (v_overflow.rshift_arith(2)) & (1 << 63) != 0  # arithmetic shift
        assert (v_overflow.rshift_arith(v_low)) & (1 << 63) != 0  # arithmetic shift
        assert (v_noMSB >> 2) == (v_noMSB.rshift_arith(2))
        assert (v1 * v2).value == (v1.value * v2.value) % StoredValue.MODULUS
        assert (v1 - v2).value == (v1.value - v2.value) % StoredValue.MODULUS
        assert (v1 & v2).value == (v1.value & v2.value)
        assert (v1 | v2).value == (v1.value | v2.value)
        assert (v1 ^ v2).value == (v1.value ^ v2.value)
        assert (v2 + v1 - v2) == v1
        assert (v_overflow + 1) == 0
        assert StoredValue(1, valid=False) == StoredValue(42, valid=False)
        assert StoredValue(0) != StoredValue(0, valid=False)
        assert StoredValue(0, accurate=True).accurate
        assert (StoredValue(0, accurate=True) + 10).accurate
        assert (StoredValue(0, accurate=True) + StoredValue(42, accurate=True)).accurate

        # Casts
        v_hilo = StoredValue.fresh()
        lo_mask = (1 << 32) - 1
        hi_mask = lo_mask << 32
        while v_hilo.value & lo_mask == 0 or v_hilo.value & hi_mask == 0:
            v_hilo = StoredValue.fresh()
        v_hilo._value |= np.uint64(1 << 31)  # to test sign extend
        assert isinstance(v_hilo.np_value, np.uint64)
        # v_hilo contains bits in both low and high 32-bits
        v_lo = v_hilo.cast(32, fromsize=64)
        assert isinstance(v_lo.np_value, np.uint32)
        # `signed` is irrelevant for downcasts
        assert v_lo == v_hilo.cast(32, fromsize=64, signed=True)
        assert v_lo.value == (v_hilo.value & lo_mask)
        assert v_lo.value & hi_mask == 0
        assert v_hilo.value & hi_mask != 0  # copy/ref problems
        v_lo_uext = v_lo.cast(64, fromsize=32, signed=False)
        assert isinstance(v_lo_uext.np_value, np.uint64)
        assert v_lo_uext.value == (v_hilo.value & lo_mask)
        assert v_lo_uext.value & hi_mask == 0
        v_lo_sext_neg = v_lo.cast(64, fromsize=32, signed=True)
        assert isinstance(v_lo_sext_neg.np_value, np.uint64)
        assert v_lo_sext_neg.value & lo_mask == (v_hilo.value & lo_mask)
        assert v_lo_sext_neg.value & hi_mask == hi_mask
        v_lo_pos = v_lo & (~(np.uint64(1 << 31)))
        v_lo_sext_pos = v_lo_pos.cast(64, fromsize=32, signed=True)
        assert v_lo_sext_pos.value & lo_mask == (v_hilo.value & (lo_mask ^ (1 << 31)))
        assert v_lo_sext_pos.value & hi_mask == 0x0

        # Type match
        v_u32 = StoredValue.fresh(typ=np.uint32)
        v_i32 = StoredValue.fresh(typ=np.int32)
        v_i64 = StoredValue.fresh(typ=np.int64)
        assert type(v1.np_value) == np.uint64
        assert type(v_u32.np_value) == np.uint32
        assert type(v_i64.np_value) == np.int64
        assert type(v_i32.np_value) == np.int32
        assert type((v1 + v2).np_value) == np.uint64  # Use type(): better pytest report
        assert type((v1 + v_u32).np_value) == np.uint64
        assert type((v_u32 + v_u32).np_value) == np.uint32
        assert type((v_i32 + v_i32).np_value) == np.int32
        assert type((v_i64 + v_i32).np_value) == np.int64
        assert type((v1.rshift_arith(3)).np_value) == np.uint64
        assert type((v_i64.rshift_arith(3)).np_value) == np.int64
        assert type((v_u32.rshift_arith(3)).np_value) == np.uint32
        assert type((v_i32.rshift_arith(3)).np_value) == np.int32
        with pytest.raises(random_values.BadTypeMerge):
            v_i64 + v1
