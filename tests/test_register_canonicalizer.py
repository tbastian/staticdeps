from staticdeps import capstone_helper
import archinfo
import capstone
import pyvex


def test_reg_canonicalizer_x86():
    can = capstone_helper.RegCanonicalizer(archinfo.ArchAMD64())

    # Canonical names
    assert can.canonical(capstone.x86.X86_REG_RAX) == capstone.x86.X86_REG_RAX
    assert can.canonical(capstone.x86.X86_REG_EAX) == capstone.x86.X86_REG_RAX
    assert can.canonical(capstone.x86.X86_REG_AX) == capstone.x86.X86_REG_RAX
    assert can.canonical(capstone.x86.X86_REG_AH) == capstone.x86.X86_REG_RAX
    assert can.canonical(capstone.x86.X86_REG_R10D) == capstone.x86.X86_REG_R10
    assert can.canonical(capstone.x86.X86_REG_XMM5) == capstone.x86.X86_REG_YMM5

    # Subregisters
    assert set(can.subregs(capstone.x86.X86_REG_RAX)) == set(
        [
            capstone.x86.X86_REG_RAX,
            capstone.x86.X86_REG_EAX,
            capstone.x86.X86_REG_AX,
            capstone.x86.X86_REG_AH,
            capstone.x86.X86_REG_AL,
        ]
    )
    assert set(can.subregs(capstone.x86.X86_REG_EAX)) == set(
        [
            capstone.x86.X86_REG_EAX,
            capstone.x86.X86_REG_AX,
            capstone.x86.X86_REG_AH,
            capstone.x86.X86_REG_AL,
        ]
    )
    assert set(can.subregs(capstone.x86.X86_REG_AL)) == set(
        [
            capstone.x86.X86_REG_AL,
        ]
    )

    assert set(can.subregs(capstone.x86.X86_REG_YMM2)) == set(
        [
            capstone.x86.X86_REG_YMM2,
            capstone.x86.X86_REG_XMM2,
        ]
    )

    # Superregisters
    assert set(can.superregs(capstone.x86.X86_REG_AL)) == set(
        [
            capstone.x86.X86_REG_RAX,
            capstone.x86.X86_REG_EAX,
            capstone.x86.X86_REG_AX,
            capstone.x86.X86_REG_AL,
        ]
    )
    assert set(can.superregs(capstone.x86.X86_REG_YMM2)) == set(
        [
            capstone.x86.X86_REG_YMM2,
        ]
    )


def test_reg_canonicalizer_aarch64():
    can = capstone_helper.RegCanonicalizer(archinfo.ArchAArch64())

    # Canonical names
    assert can.canonical(capstone.arm64.ARM64_REG_X12) == capstone.arm64.ARM64_REG_X12
    assert can.canonical(capstone.arm64.ARM64_REG_W12) == capstone.arm64.ARM64_REG_X12
    assert can.canonical(capstone.arm64.ARM64_REG_Q3) == capstone.arm64.ARM64_REG_Q3
    assert can.canonical(capstone.arm64.ARM64_REG_D3) == capstone.arm64.ARM64_REG_Q3
    assert can.canonical(capstone.arm64.ARM64_REG_S3) == capstone.arm64.ARM64_REG_Q3
    assert can.canonical(capstone.arm64.ARM64_REG_H3) == capstone.arm64.ARM64_REG_Q3
    assert can.canonical(capstone.arm64.ARM64_REG_B3) == capstone.arm64.ARM64_REG_Q3

    # Subregisters
    assert set(can.subregs(capstone.arm64.ARM64_REG_X12)) == set(
        [
            capstone.arm64.ARM64_REG_X12,
            capstone.arm64.ARM64_REG_W12,
        ]
    )
    assert set(can.subregs(capstone.arm64.ARM64_REG_W12)) == set(
        [
            capstone.arm64.ARM64_REG_W12,
        ]
    )

    assert set(can.subregs(capstone.arm64.ARM64_REG_Q3)) == set(
        [
            capstone.arm64.ARM64_REG_Q3,
            capstone.arm64.ARM64_REG_V3,
            capstone.arm64.ARM64_REG_D3,
            capstone.arm64.ARM64_REG_S3,
            capstone.arm64.ARM64_REG_H3,
            capstone.arm64.ARM64_REG_B3,
        ]
    )

    # Superregisters
    assert set(can.superregs(capstone.arm64.ARM64_REG_W12)) == set(
        [
            capstone.arm64.ARM64_REG_W12,
            capstone.arm64.ARM64_REG_X12,
        ]
    )
    assert set(can.superregs(capstone.arm64.ARM64_REG_B3)) == set(
        [
            capstone.arm64.ARM64_REG_Q3,
            capstone.arm64.ARM64_REG_V3,
            capstone.arm64.ARM64_REG_D3,
            capstone.arm64.ARM64_REG_S3,
            capstone.arm64.ARM64_REG_H3,
            capstone.arm64.ARM64_REG_B3,
        ]
    )
